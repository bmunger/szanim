#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#include <Seizyur.h>
using namespace Seizyur::Math;
using namespace Seizyur::Debug;
using Seizyur::Core::traits;

namespace SzMathTest
{
	TEST_CLASS(MatrixArithmeticUnitTest)
	{
	public:
		
		TEST_METHOD(Add) { }
		TEST_METHOD(Sub) { }
		TEST_METHOD(MulByScalar) { }
		TEST_METHOD(MulCwise) { }
		TEST_METHOD(MulOuter) { }
		TEST_METHOD(DivByScalar) { }
		TEST_METHOD(DivCwise) { }

		TEST_METHOD(operator_compound_add) { }
		TEST_METHOD(operator_compound_sub) { }
		TEST_METHOD(operator_compound_mul_scalar) { }
		TEST_METHOD(operator_compound_mul_matrix) { }
		TEST_METHOD(operator_compound_div_scalar) { }
		TEST_METHOD(operator_arithmetic_add) { }
		TEST_METHOD(operator_arithmetic_sub) { }
		TEST_METHOD(operator_arithmetic_mul_matrix_scalar) { }
		TEST_METHOD(operator_arithmetic_mul_scalar_matrix) { }
		TEST_METHOD(operator_arithmetic_mul_matrix_matrix) { }
		TEST_METHOD(operator_arithmetic_div_scalar) { }
	};
}