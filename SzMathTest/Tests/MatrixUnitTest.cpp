#include "stdafx.h"
#include "CppUnitTest.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#include <Seizyur.h>
using namespace Seizyur::Math;
using namespace Seizyur::Debug;
using Seizyur::Core::traits;

namespace SzMathTest
{
	TEST_CLASS(MatrixUnitTest)
	{
	public:

		TEST_METHOD(default_constructor)
		{
			TestDefaultConstructor<3, 3, float>();
			TestDefaultConstructor<3, 10, double>();
			TestDefaultConstructor<4, 3, int>();
			TestDefaultConstructor<2, 1, float>();
			TestDefaultConstructor<1, 3, float>();
			// 			TestDefaultConstructor<0, 0, float>(); // Correctly causes C2503
			// 			TestDefaultConstructor<0, 1, float>(); // Correctly causes C2503
			// 			TestDefaultConstructor<1, 0, float>(); // Correctly causes C2503
		}

		TEST_METHOD(copy_constructor)
		{
			TestCopyConstructor<3, 3, float>();
			TestCopyConstructor<3, 10, double>();
			TestCopyConstructor<4, 3, int>();
			TestCopyConstructor<2, 1, float>();
			TestCopyConstructor<1, 3, float>();
		}

		TEST_METHOD(Identity)
		{
			std::ostringstream ostr;

			Print(ostr, "Matrix<4, 4, float>::Identity()", Matrix<4, 4, float>::Identity());
			Print(ostr, "Matrix<4, 2, float>::Identity()", Matrix<4, 2, float>::Identity());
			Print(ostr, "Matrix<2, 4, float>::Identity()", Matrix<2, 4, float>::Identity());

			auto ident = Matrix<2, 4, float>::Identity();
			ident(0, 2) = 15;
			Print(ostr, "ident", ident);
			Print(ostr, "Matrix<2, 4, float>::Identity()", Matrix<2, 4, float>::Identity());

			Logger::WriteMessage(ostr.str().c_str());
		}

		TEST_METHOD(Diagonal)
		{
			std::ostringstream ostr;

			Print(ostr, "Matrix<4, 4, float>::Diagonal(12)", Matrix<4, 4, float>::Diagonal(12));
			Print(ostr, "Matrix<4, 2, float>::Diagonal(12)", Matrix<4, 2, float>::Diagonal(12));
			Print(ostr, "Matrix<2, 4, float>::Diagonal(12)", Matrix<2, 4, float>::Diagonal(12));

			Logger::WriteMessage(ostr.str().c_str());
		}

		TEST_METHOD(Equality)
		{
			TestEquality<Matrix<4, 4, float>, Matrix<4, 4, float> >();

			// TODO: Make convertible types comparable.
			// 			TestEquality<Matrix<4, 4, float>, Matrix<4, 4, double> >();

			TestEquality<Matrix<2, 4, double>, Matrix<2, 4, double> >();
			TestEquality<Matrix<4, 2, float>, Matrix<4, 2, float> >();
		}

		TEST_METHOD(Rows)
		{
			Assert::AreEqual(3, Matrix < 3, 4, float > {}.Rows());
			Assert::AreEqual(4, Matrix < 4, 4, float > {}.Rows());
			Assert::AreNotEqual(4, Matrix < 10, 2, float > {}.Rows());
			Assert::AreNotEqual(4, Matrix < 10, 4, float > {}.Rows());
		}
		TEST_METHOD(Columns)
		{
			Assert::AreEqual(3, Matrix < 4, 3, float > {}.Columns());
			Assert::AreEqual(4, Matrix < 4, 4, double > {}.Columns());
			Assert::AreNotEqual(4, Matrix < 2, 10, float > {}.Columns());
			Assert::AreNotEqual(4, Matrix < 4, 10, float > {}.Columns());
		}

		TEST_METHOD(GetRow)
		{
			TestGetRow<Matrix<4, 4, float>>(2);
			TestGetRow<Matrix<4, 7, double>>(1);
			TestGetRow<Matrix<7, 3, float>>(2);
			TestGetRow<Matrix<1, 1, float>>(0);
			TestGetRow<Matrix<1, 6, float>>(0);
			TestGetRow<Matrix<6, 1, float>>(2);
		}

		TEST_METHOD(GetColumn)
		{
			TestGetColumn<Matrix<4, 4, float>>(2);
			TestGetColumn<Matrix<4, 7, double>>(1);
			TestGetColumn<Matrix<7, 3, float>>(2);
			TestGetColumn<Matrix<1, 1, float>>(0);
			TestGetColumn<Matrix<1, 6, float>>(4);
			TestGetColumn<Matrix<6, 1, float>>(0);
		}

		TEST_METHOD(GetTranspose)
		{
			TestGetTranspose<Matrix<4, 4, float>>();
			TestGetTranspose<Matrix<4, 7, double>>();
			TestGetTranspose<Matrix<7, 3, float>>();
			TestGetTranspose<Matrix<1, 1, float>>();
			TestGetTranspose<Matrix<1, 6, float>>();
			TestGetTranspose<Matrix<6, 1, float>>();
		}

		TEST_METHOD(Set_Scalar) { }
		TEST_METHOD(Set_Matrix) { }
		TEST_METHOD(Set_SubMatrix) { }
		TEST_METHOD(SetDiagonal) { }
		TEST_METHOD(SetZero) { }
		TEST_METHOD(SetIdentity) { }

		TEST_METHOD(operator_assignment) { }
		TEST_METHOD(operator_accessor_paren_1d) { }
		TEST_METHOD(operator_accessor_paren_1d_const) { }
		TEST_METHOD(operator_accessor_paren_2d) { }
		TEST_METHOD(operator_accessor_paren_2d_const) { }

	private:
		template<typename _T>
		void TestGetTranspose()
		{
			std::ostringstream ostr;
			Print(ostr, "_T::Diagonal().GetTranspose() before", _T::Diagonal(12));
			Print(ostr, "_T::Diagonal().GetTranspose() after", _T::Diagonal(12).GetTranspose());
			Logger::WriteMessage(ostr.str().c_str());
		}

		template<typename _T>
		void TestGetRow(int row)
		{
			std::ostringstream ostr;
			Print(ostr, "_T::Diagonal().GetRow()", _T::Diagonal(12).GetRow(row));
			Logger::WriteMessage(ostr.str().c_str());
		}

		template<typename _T>
		void TestGetColumn(int col)
		{
			std::ostringstream ostr;
			Print(ostr, "_T::Diagonal().GetColumn()", _T::Diagonal(12).GetColumn(col));
			Logger::WriteMessage(ostr.str().c_str());
		}

		template<typename _T1, typename _T2>
		void TestEquality()
		{
			auto ident1 = _T1::Identity();
			auto ident2 = _T2::Identity();

			Assert::AreEqual(true, ident1.IsEqual(ident1));
			Assert::AreEqual(true, ident1 == ident1);

			Assert::AreEqual(true, ident1.IsEqual(ident2));
			Assert::AreEqual(true, ident2.IsEqual(ident1));

			Assert::AreEqual(true, ident1 == ident2);
			Assert::AreEqual(true, ident2 == ident1);

			Assert::AreEqual(false, ident1 != ident2);
			Assert::AreEqual(false, ident2 != ident1);

			auto diag1 = _T1::Diagonal(12.0f);
			Assert::AreEqual(false, ident1.IsEqual(diag1));
			Assert::AreEqual(false, diag1.IsEqual(ident1));
			Assert::AreEqual(false, ident1 == diag1);
			Assert::AreEqual(false, diag1 == ident1);
			Assert::AreEqual(true, ident1 != diag1);
			Assert::AreEqual(true, diag1 != ident1);
		}

		template<int _ROWS, int _COLUMNS, typename _TYPE>
		void TestDefaultConstructor()
		{
			const _TYPE one = traits<_TYPE>::one();
			const _TYPE two = one + one;
			const _TYPE three = two + one;
			const _TYPE four = three + one;
			const _TYPE five = four + one;

			std::ostringstream ostr;

			Matrix<_ROWS, _COLUMNS, _TYPE> mat1;
			Print(ostr, "mat1", mat1);

			Matrix<_ROWS, _COLUMNS, _TYPE> mat2{ };
			Print(ostr, "mat2 { }", mat2);

			Matrix<_ROWS, _COLUMNS, _TYPE> mat3{ one };
			Print(ostr, "mat3 { 1 }", mat3);

			Matrix<_ROWS, _COLUMNS, _TYPE> mat4{ one, two };
			Print(ostr, "mat4 { 1, 2 }", mat4);

			Matrix<_ROWS, _COLUMNS, _TYPE> mat5(three);
			Print(ostr, "mat5 (3)", mat5);

			Matrix<_ROWS, _COLUMNS, _TYPE> mat6(three, four);
			Print(ostr, "mat6 (3, 4)", mat6);

			Matrix<_ROWS, _COLUMNS, _TYPE> mat7({ four, five });
			Print(ostr, "mat7 ({ 4, 5 })", mat7);

			Logger::WriteMessage(ostr.str().c_str());
		}

		template<int _ROWS, int _COLUMNS, typename _TYPE>
		void TestCopyConstructor()
		{
			const _TYPE one = traits<_TYPE>::one();
			const _TYPE two = one + one;
			const _TYPE three = two + one;
			const _TYPE four = three + one;
			const _TYPE five = four + one;

			std::ostringstream ostr;

			Matrix<_ROWS, _COLUMNS, _TYPE> mat1{ one, three };
			Print(ostr, "mat1{ one, three }", mat1);

			Matrix<_ROWS, _COLUMNS, _TYPE> mat2{ mat1 };
			Print(ostr, "mat2{ mat1 }", mat2);

			Matrix<_ROWS, _COLUMNS, _TYPE> mat3(mat2);
			Print(ostr, "mat3(mat2)", mat3);

			Matrix<_ROWS, _COLUMNS, _TYPE> mat4({ mat3 });
			Print(ostr, "mat4({mat3})", mat4);

			//TODO: Implement initializing matrices with different size matrices (?)
			// 			Matrix<_ROWS+1, _COLUMNS+1, _TYPE> mat5(mat4);
			// 			Print(ostr, "mat5(mat4)", mat5);

			Logger::WriteMessage(ostr.str().c_str());
		}
	};
}