#include "stdafx.h"
#include "CppUnitTest.h"

#include <Seizyur.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Seizyur::Core;

namespace SzMathTest
{
	TEST_CLASS(Array2dAccessorUnitTest)
	{
		enum class AccessType
		{
			None,
			Paren1d,
			Paren2d,
			Bracket
		};
	public:

		template<typename _TYPE>
		void RunAccessorTest(AccessType setType, AccessType getType)
		{
			_TYPE obj;
			typedef typename traits<_TYPE>::value_t scalar_t;

			switch(setType)
			{
			case AccessType::Paren1d:
				for(int i = 0; i < obj.Size(); i++)
					obj(i) = static_cast<scalar_t>(i);
				break;
			case AccessType::Paren2d:
				for(int xIdx = 0; xIdx < obj.Width(); xIdx++)
					for(int yIdx = 0; yIdx < obj.Height(); yIdx++)
						obj(xIdx, yIdx) = static_cast<scalar_t>(xIdx + yIdx * obj.Width());
				break;
			case AccessType::Bracket:
				for(int i = 0; i < obj.Size(); i++)
					obj[i] = static_cast<scalar_t>(i);
				break;
			}

			switch(getType)
			{
			case AccessType::Paren1d:
				for(int i = 0; i < obj.Size(); i++)
					Assert::AreEqual(obj(i), static_cast<scalar_t>(i));
				break;
			case AccessType::Paren2d:
				for(int xIdx = 0; xIdx < obj.Width(); xIdx++)
					for(int yIdx = 0; yIdx < obj.Height(); yIdx++)
						Assert::AreEqual(obj(xIdx, yIdx), static_cast<scalar_t>(xIdx + yIdx * obj.Width()));
				break;
			case AccessType::Bracket:
				for(int i = 0; i < obj.Size(); i++)
					Assert::AreEqual(obj[i], static_cast<scalar_t>(i));
				break;
			}
		}

		template<typename _SCALAR>
		void RunTestGetSet(AccessType getter, AccessType setter)
		{
			RunAccessorTest< Array2d<1, 94, _SCALAR> >(setter, getter);
			RunAccessorTest< Array2d<94, 1, _SCALAR> >(setter, getter);
			RunAccessorTest< Array2d<10, 9, _SCALAR> >(setter, getter);
			RunAccessorTest< Array2d< 3, 3, _SCALAR> >(setter, getter);
			RunAccessorTest< Array2d< 2, 2, _SCALAR> >(setter, getter);
		}

		template<typename _SCALAR>
		void RunTestConstGetSet(AccessType getter)
		{
			const _SCALAR one = traits<const _SCALAR>::one();
			Array2d<2, 3, const _SCALAR> obj{ one, one, one, one, one, one };
			for(int xIdx = 0; xIdx < obj.Width(); xIdx++)
			{
				for(int yIdx = 0; yIdx < obj.Height(); yIdx++)
				{
					switch(getter)
					{
					case AccessType::Paren1d:
						Assert::AreEqual(obj(xIdx + yIdx * obj.Width()), one);
						break;
					case AccessType::Paren2d:
						Assert::AreEqual(obj(xIdx, yIdx), one);
						break;
					case AccessType::Bracket:
						Assert::AreEqual(obj[xIdx + yIdx * obj.Width()], one);
						break;
					}
				}
			}
		}

		TEST_METHOD(SetByParen1d_GetByParen1d_flt) { RunTestGetSet<float>(AccessType::Paren1d, AccessType::Paren1d); }
		TEST_METHOD(SetByParen1d_GetByParen2d_flt) { RunTestGetSet<float>(AccessType::Paren1d, AccessType::Paren2d); }
		TEST_METHOD(SetByParen1d_GetByBracket_flt) { RunTestGetSet<float>(AccessType::Paren1d, AccessType::Bracket); }
		TEST_METHOD(SetByParen2d_GetByParen1d_flt) { RunTestGetSet<float>(AccessType::Paren2d, AccessType::Paren1d); }
		TEST_METHOD(SetByParen2d_GetByParen2d_flt) { RunTestGetSet<float>(AccessType::Paren2d, AccessType::Paren2d); }
		TEST_METHOD(SetByParen2d_GetByBracket_flt) { RunTestGetSet<float>(AccessType::Paren2d, AccessType::Bracket); }
		TEST_METHOD(SetByBracket_GetByParen1d_flt) { RunTestGetSet<float>(AccessType::Bracket, AccessType::Paren1d); }
		TEST_METHOD(SetByBracket_GetByParen2d_flt) { RunTestGetSet<float>(AccessType::Bracket, AccessType::Paren2d); }
		TEST_METHOD(SetByBracket_GetByBracket_flt) { RunTestGetSet<float>(AccessType::Bracket, AccessType::Bracket); }

		TEST_METHOD(SetByParen1d_GetByParen1d_dbl) { RunTestGetSet<double>(AccessType::Paren1d, AccessType::Paren1d); }
		TEST_METHOD(SetByParen1d_GetByParen2d_dbl) { RunTestGetSet<double>(AccessType::Paren1d, AccessType::Paren2d); }
		TEST_METHOD(SetByParen1d_GetByBracket_dbl) { RunTestGetSet<double>(AccessType::Paren1d, AccessType::Bracket); }
		TEST_METHOD(SetByParen2d_GetByParen1d_dbl) { RunTestGetSet<double>(AccessType::Paren2d, AccessType::Paren1d); }
		TEST_METHOD(SetByParen2d_GetByParen2d_dbl) { RunTestGetSet<double>(AccessType::Paren2d, AccessType::Paren2d); }
		TEST_METHOD(SetByParen2d_GetByBracket_dbl) { RunTestGetSet<double>(AccessType::Paren2d, AccessType::Bracket); }
		TEST_METHOD(SetByBracket_GetByParen1d_dbl) { RunTestGetSet<double>(AccessType::Bracket, AccessType::Paren1d); }
		TEST_METHOD(SetByBracket_GetByParen2d_dbl) { RunTestGetSet<double>(AccessType::Bracket, AccessType::Paren2d); }
		TEST_METHOD(SetByBracket_GetByBracket_dbl) { RunTestGetSet<double>(AccessType::Bracket, AccessType::Bracket); }

		TEST_METHOD(SetByParen1d_GetByParen1d_int) { RunTestGetSet<int>(AccessType::Paren1d, AccessType::Paren1d); }
		TEST_METHOD(SetByParen1d_GetByParen2d_int) { RunTestGetSet<int>(AccessType::Paren1d, AccessType::Paren2d); }
		TEST_METHOD(SetByParen1d_GetByBracket_int) { RunTestGetSet<int>(AccessType::Paren1d, AccessType::Bracket); }
		TEST_METHOD(SetByParen2d_GetByParen1d_int) { RunTestGetSet<int>(AccessType::Paren2d, AccessType::Paren1d); }
		TEST_METHOD(SetByParen2d_GetByParen2d_int) { RunTestGetSet<int>(AccessType::Paren2d, AccessType::Paren2d); }
		TEST_METHOD(SetByParen2d_GetByBracket_int) { RunTestGetSet<int>(AccessType::Paren2d, AccessType::Bracket); }
		TEST_METHOD(SetByBracket_GetByParen1d_int) { RunTestGetSet<int>(AccessType::Bracket, AccessType::Paren1d); }
		TEST_METHOD(SetByBracket_GetByParen2d_int) { RunTestGetSet<int>(AccessType::Bracket, AccessType::Paren2d); }
		TEST_METHOD(SetByBracket_GetByBracket_int) { RunTestGetSet<int>(AccessType::Bracket, AccessType::Bracket); }

		TEST_METHOD(ConstInit_GetByParen1d_float) { RunTestConstGetSet<float>(AccessType::Paren1d); }
		TEST_METHOD(ConstInit_GetByParen2d_float) { RunTestConstGetSet<float>(AccessType::Paren2d); }
		TEST_METHOD(ConstInit_GetByBracket_float) { RunTestConstGetSet<float>(AccessType::Bracket); }
		TEST_METHOD(ConstInit_GetByParen1d_double) { RunTestConstGetSet<double>(AccessType::Paren1d); }
		TEST_METHOD(ConstInit_GetByParen2d_double) { RunTestConstGetSet<double>(AccessType::Paren2d); }
		TEST_METHOD(ConstInit_GetByBracket_double) { RunTestConstGetSet<double>(AccessType::Bracket); }
		TEST_METHOD(ConstInit_GetByParen1d_int) { RunTestConstGetSet<int>(AccessType::Paren1d); }
		TEST_METHOD(ConstInit_GetByParen2d_int) { RunTestConstGetSet<int>(AccessType::Paren2d); }
		TEST_METHOD(ConstInit_GetByBracket_int) { RunTestConstGetSet<int>(AccessType::Bracket); }
	};
}