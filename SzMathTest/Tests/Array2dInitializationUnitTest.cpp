#include "stdafx.h"
#include "CppUnitTest.h"

#include <Seizyur.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Seizyur::Core;

namespace SzMathTest
{
	TEST_CLASS(Array2dInitializationUnitTest)
	{
	public:
		template<int _WIDTH, int _HEIGHT, typename _TYPE>
		void RunDefaultInitializationTest()
		{
			Array2d<_WIDTH, _HEIGHT, _TYPE> obj;
			obj.Fill(traits<_TYPE>::zero());

			for(int i = 0; i < obj.Width(); i++)
				for(int j = 0; j < obj.Height(); j++)
					Assert::AreEqual(obj(i, j), traits<_TYPE>::zero());
		}
		TEST_METHOD(DefaultInitialization)
		{
			RunDefaultInitializationTest<1, 91, float>();
			RunDefaultInitializationTest<3, 3, double>();
			RunDefaultInitializationTest<10, 2, int>();
		}

		template<typename _T>
		void Run_direct_curly_scalar()
		{
			SZ_STATIC_ASSERT(traits<_T>::size >= 5, "Array2d array must be have at least five elements.");

			typedef typename traits<_T>::value_t scalar_t;

			scalar_t val1 = traits<scalar_t>::one();
			scalar_t val2 = val1 + val1;
			scalar_t val3 = val2 + val1;
			scalar_t val4 = val3 + val1;
			scalar_t val5 = val4 + val1;

			_T obj{ val1, val2, val3, val4, val5 };
			Assert::AreEqual(obj(0), val1);
			Assert::AreEqual(obj(1), val2);
			Assert::AreEqual(obj(2), val3);
			Assert::AreEqual(obj(3), val4);
			Assert::AreEqual(obj(4), val5);

			for(int idx = 5; idx < obj.Size(); idx++)
				Assert::AreEqual(obj(idx), traits<scalar_t>::zero());
		}

		template<typename _T>
		void Run_direct_curly_scalar_empty()
		{
			typedef typename traits<_T>::value_t scalar_t;

			_T obj{ };
			for(int idx = 0; idx < obj.Size(); idx++)
				Assert::AreEqual(obj(idx), traits<scalar_t>::zero());
		}

		TEST_METHOD(Direct_curly_complete_float) { Run_direct_curly_scalar<Array2d<1, 5, float>>(); }
		TEST_METHOD(Direct_curly_complete_int) { Run_direct_curly_scalar<Array2d<1, 5, int>>(); }
		TEST_METHOD(Direct_curly_complete_double) { Run_direct_curly_scalar<Array2d<1, 5, double>>(); }

		TEST_METHOD(Direct_curly_complete_const_float) { Run_direct_curly_scalar<const Array2d<5, 1, float>>(); }
		TEST_METHOD(Direct_curly_complete_const_int) { Run_direct_curly_scalar<const Array2d<5, 1, int>>(); }
		TEST_METHOD(Direct_curly_complete_const_double) { Run_direct_curly_scalar<const Array2d<5, 1, double>>(); }

		TEST_METHOD(Direct_curly_complete_float_const) { Run_direct_curly_scalar<Array2d<1, 5, const float>>(); }
		TEST_METHOD(Direct_curly_complete_int_const) { Run_direct_curly_scalar<Array2d<1, 5, const int>>(); }
		TEST_METHOD(Direct_curly_complete_double_const) { Run_direct_curly_scalar<Array2d<1, 5, const double>>(); }

		TEST_METHOD(Direct_curly_complete_const_float_const) { Run_direct_curly_scalar<const Array2d<1, 5, const float>>(); }
		TEST_METHOD(Direct_curly_complete_const_int_const) { Run_direct_curly_scalar<const Array2d<1, 5, const int>>(); }
		TEST_METHOD(Direct_curly_complete_const_double_const) { Run_direct_curly_scalar<const Array2d<1, 5, const double>>(); }

		TEST_METHOD(Direct_curly_partial_float) { Run_direct_curly_scalar<Array2d<12, 2, float>>(); }
		TEST_METHOD(Direct_curly_partial_int) { Run_direct_curly_scalar<Array2d<12, 2, int>>(); }
		TEST_METHOD(Direct_curly_partial_double) { Run_direct_curly_scalar<Array2d<12, 2, double>>(); }

		TEST_METHOD(Direct_curly_partial_float_const) { Run_direct_curly_scalar<Array2d<12, 2, const float>>(); }
		TEST_METHOD(Direct_curly_partial_int_const) { Run_direct_curly_scalar<Array2d<12, 2, const int>>(); }
		TEST_METHOD(Direct_curly_partial_double_const) { Run_direct_curly_scalar<Array2d<12, 2, const double>>(); }

		TEST_METHOD(Direct_curly_partial_const_float) { Run_direct_curly_scalar<const Array2d<12, 2, float>>(); }
		TEST_METHOD(Direct_curly_partial_const_int) { Run_direct_curly_scalar<const Array2d<12, 2, int>>(); }
		TEST_METHOD(Direct_curly_partial_const_double) { Run_direct_curly_scalar<const Array2d<12, 2, double>>(); }

		TEST_METHOD(Direct_curly_partial_const_float_const) { Run_direct_curly_scalar<const Array2d<12, 2, const float>>(); }
		TEST_METHOD(Direct_curly_partial_const_int_const) { Run_direct_curly_scalar<const Array2d<12, 2, const int>>(); }
		TEST_METHOD(Direct_curly_partial_const_double_const) { Run_direct_curly_scalar<const Array2d<12, 2, const double>>(); }

		TEST_METHOD(Direct_curly_empty_float) { Run_direct_curly_scalar_empty<Array2d<12, 2, float>>(); }
		TEST_METHOD(Direct_curly_empty_int) { Run_direct_curly_scalar_empty<Array2d<12, 2, int>>(); }
		TEST_METHOD(Direct_curly_empty_double) { Run_direct_curly_scalar_empty<Array2d<12, 2, double>>(); }

		TEST_METHOD(Direct_curly_empty_float_const) { /*Run_direct_curly_scalar_empty<Array2d<12, 2, const float>>();*/ }
		TEST_METHOD(Direct_curly_empty_int_const) { /*Run_direct_curly_scalar_empty<Array2d<12, 2, const int>>();*/ }
		TEST_METHOD(Direct_curly_empty_double_const) { /*Run_direct_curly_scalar_empty<Array2d<12, 2, const double>>();*/ }

		TEST_METHOD(Direct_curly_empty_const_float) { Run_direct_curly_scalar_empty<const Array2d<12, 2, float>>(); }
		TEST_METHOD(Direct_curly_empty_const_int) { Run_direct_curly_scalar_empty<const Array2d<12, 2, int>>(); }
		TEST_METHOD(Direct_curly_empty_const_double) { Run_direct_curly_scalar_empty<const Array2d<12, 2, double>>(); }

		TEST_METHOD(Direct_curly_empty_const_float_const) { /*Run_direct_curly_scalar_empty<const Array2d<12, 2, const float>>();*/ }
		TEST_METHOD(Direct_curly_empty_const_int_const) { /*Run_direct_curly_scalar_empty<const Array2d<12, 2, const int>>();*/ }
		TEST_METHOD(Direct_curly_empty_const_double_const) { /*Run_direct_curly_scalar_empty<const Array2d<12, 2, const double>>();*/ }

		template<typename _T>
		void Run_direct_paren_scalar()
		{
			SZ_STATIC_ASSERT(traits<_T>::size >= 5, "Array2d array must be have at least five elements.");

			typedef typename traits<_T>::value_t scalar_t;

			scalar_t val1 = traits<scalar_t>::one();
			scalar_t val2 = val1 + val1;
			scalar_t val3 = val2 + val1;
			scalar_t val4 = val3 + val1;
			scalar_t val5 = val4 + val1;

			_T obj{ val1, val2, val3, val4, val5 };
			Assert::AreEqual(obj(0), val1);
			Assert::AreEqual(obj(1), val2);
			Assert::AreEqual(obj(2), val3);
			Assert::AreEqual(obj(3), val4);
			Assert::AreEqual(obj(4), val5);

			for(int idx = 5; idx < obj.Size(); idx++)
				Assert::AreEqual(obj(idx), traits<scalar_t>::zero());
		}

		TEST_METHOD(Direct_paren_complete_float) { Run_direct_paren_scalar<Array2d<1, 5, float>>(); }
		TEST_METHOD(Direct_paren_complete_int) { Run_direct_paren_scalar<Array2d<1, 5, int>>(); }
		TEST_METHOD(Direct_paren_complete_double) { Run_direct_paren_scalar<Array2d<1, 5, double>>(); }

		TEST_METHOD(Direct_paren_complete_const_float) { Run_direct_paren_scalar<const Array2d<5, 1, float>>(); }
		TEST_METHOD(Direct_paren_complete_const_int) { Run_direct_paren_scalar<const Array2d<5, 1, int>>(); }
		TEST_METHOD(Direct_paren_complete_const_double) { Run_direct_paren_scalar<const Array2d<5, 1, double>>(); }

		TEST_METHOD(Direct_paren_complete_float_const) { Run_direct_paren_scalar<Array2d<1, 5, const float>>(); }
		TEST_METHOD(Direct_paren_complete_int_const) { Run_direct_paren_scalar<Array2d<1, 5, const int>>(); }
		TEST_METHOD(Direct_paren_complete_double_const) { Run_direct_paren_scalar<Array2d<1, 5, const double>>(); }

		TEST_METHOD(Direct_paren_complete_const_float_const) { Run_direct_paren_scalar<const Array2d<1, 5, const float>>(); }
		TEST_METHOD(Direct_paren_complete_const_int_const) { Run_direct_paren_scalar<const Array2d<1, 5, const int>>(); }
		TEST_METHOD(Direct_paren_complete_const_double_const) { Run_direct_paren_scalar<const Array2d<1, 5, const double>>(); }

		TEST_METHOD(Direct_paren_partial_float) { Run_direct_paren_scalar<Array2d<12, 2, float>>(); }
		TEST_METHOD(Direct_paren_partial_int) { Run_direct_paren_scalar<Array2d<12, 2, int>>(); }
		TEST_METHOD(Direct_paren_partial_double) { Run_direct_paren_scalar<Array2d<12, 2, double>>(); }

		TEST_METHOD(Direct_paren_partial_float_const) { Run_direct_paren_scalar<Array2d<12, 2, const float>>(); }
		TEST_METHOD(Direct_paren_partial_int_const) { Run_direct_paren_scalar<Array2d<12, 2, const int>>(); }
		TEST_METHOD(Direct_paren_partial_double_const) { Run_direct_paren_scalar<Array2d<12, 2, const double>>(); }

		TEST_METHOD(Direct_paren_partial_const_float) { Run_direct_paren_scalar<const Array2d<12, 2, float>>(); }
		TEST_METHOD(Direct_paren_partial_const_int) { Run_direct_paren_scalar<const Array2d<12, 2, int>>(); }
		TEST_METHOD(Direct_paren_partial_const_double) { Run_direct_paren_scalar<const Array2d<12, 2, double>>(); }

		TEST_METHOD(Direct_paren_partial_const_float_const) { Run_direct_paren_scalar<const Array2d<12, 2, const float>>(); }
		TEST_METHOD(Direct_paren_partial_const_int_const) { Run_direct_paren_scalar<const Array2d<12, 2, const int>>(); }
		TEST_METHOD(Direct_paren_partial_const_double_const) { Run_direct_paren_scalar<const Array2d<12, 2, const double>>(); }
	};
}
