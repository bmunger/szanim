#ifndef SEIZYUR_DOC_H_NAMESPACES
#define SEIZYUR_DOC_H_NAMESPACES

//////////////////////////////////////////////////////////////////////////
// 
// Example namespace structure for the library.
// 
//////////////////////////////////////////////////////////////////////////

namespace Seizyur_Template
{
	namespace Core
	{
		// Debug
		// Time, Timers, etc.
	}
	namespace Math
	{
		// Vector, Matrix, Quaternion, Transform, etc.
		// General purpose ops.
		namespace Internal
		{
			// Base classes, TypeTraits, etc.
		}
	}
	namespace Animation
	{
		// Derived classes.
		namespace Internal
		{
			// Base classes.
		}
	}
	namespace Vision {}
	namespace Phys {}
	namespace IO {}
	namespace Graphics {}
	namespace Network {}
}
#endif // SEIZYUR_DOC_H_NAMESPACES