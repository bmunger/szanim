#include "stdafx.h"

#include "Source/TestRunner.h"

using Seizyur::Tests::TestRunner;

int _tmain(int argc, _TCHAR* argv[])
{
	TestRunner::Run();
	return 0;
}
