#ifndef SEIZYUR_TEST_H_QUATERNION
#define SEIZYUR_TEST_H_QUATERNION

#include "../TestBase.h"

BEGIN_NAMESPACE(Seizyur, Tests, Math)

class QuaternionTest : public TestBase
{
public:
	static bool Run();

private:

	void TestConstructors();
	template<typename _QUATTYPE, typename _SCALARTYPE> void TestConstructor(const char *logString);

	void TestInitialization();

	void TestArithmeticOperators();
	template<typename _QUATTYPE, typename _SCALARTYPE> void TestArithmeticOperator(const char *logString);

	void TestConversionOperators();

	void TestMethods();
	void TestStaticFunctions();


};

END_NAMESPACE(Seizyur, Tests, Math)

#endif // SEIZYUR_TEST_H_QUATERNION
