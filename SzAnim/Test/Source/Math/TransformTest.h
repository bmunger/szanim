#ifndef SEIZYUR_TEST_H_TRANSFORM
#define SEIZYUR_TEST_H_TRANSFORM

#include "../TestBase.h"

BEGIN_NAMESPACE(Seizyur, Tests, Math)

class TransformTest : public TestBase
{
public:
	static bool Run();

private:
	void TestInitialization();
};

END_NAMESPACE(Seizyur, Tests, Math)

#endif // SEIZYUR_TEST_H_TRANSFORM
