#ifndef SEIZYUR_TEST_H_MATRIXBASE
#define SEIZYUR_TEST_H_MATRIXBASE

#include "../TestBase.h"

BEGIN_NAMESPACE(Seizyur, Tests, Math)

class MatrixTest : public TestBase
{
public:
	static bool Run();

private:
	bool TestConstruction();
	bool TestAssignment();

	bool TestTranspose();

	bool TestAddition();

	bool TestScalarMul();
	bool TestScalarDiv();

	bool TestMatrixMul();
	bool TestMatrixMul2();
	bool TestMatrixMul3();
	bool TestInverse();

	bool TestDoolittle();
	bool TestSolver();
	bool TestSubMatrix();
};

END_NAMESPACE(Seizyur, Tests, Math)
#endif // SEIZYUR_TEST_H_MATRIXBASE
