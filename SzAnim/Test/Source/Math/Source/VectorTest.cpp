#include "../VectorTest.h"

using namespace Seizyur::Math::Geometry;

using std::cout;
using std::endl;

BEGIN_NAMESPACE(Seizyur, Tests, Math)

bool VectorTest::Run()
{
	VectorTest tests;
	tests.TestInitialization();
	tests.TestAddition();
	tests.TestDot();
	tests.TestCross();
	tests.TestMagnitude();
	tests.TestSqrMagnitude();
	tests.TestNormalized();
	tests.TestNormalize();
	return true;
}

void VectorTest::TestInitialization()
{
	Vector3f vec1{};
	vec1.x() = 4;
	cout << "\n\tVector3f vec1{};\n\tvec1.x() = 4;" << vec1;

	Vector3f vec2{ 1.0f, 2.0f, 3.0f };
	vec2.x() = 4;
	vec2[2] = 12345;
	cout << "\n\tVector3f vec2{ 1.0f, 2.0f, 3.0f };\n\tvec2.x() = 4;\n\tvec2.m(2) = 12345;" << vec2;

	const Vector3f vec3{ 1.0f, 2.0f, 3.0f };
	cout << "\n\tconst Vector3f vec3{ 1.0f, 2.0f, 3.0f };" << vec3;

	vec2 = vec1;
	cout << "\n\tvec2 = vec1;" << vec2;

	vec2 = vec3;
	cout << "\n\tvec2 = vec3;" << vec2;

	vec2.x() = 999;
	cout << "\n\tvec2.x() = 999;" << vec2;
	cout << "\n\tvec3 after vec2.x() = 999;" << vec3;

	Vector3f vec4{ vec1 };
	cout << "\n\tVector3f vec4{ vec1 };" << vec4;

	auto vec5(vec1);
	cout << "\n\tauto vec5(vec1);" << vec5;

	Vector3f vec6(1.0f, 1.0f, 1.0f);
	cout << "\n\tVector3f vec6(1.0f,1.0f,1.0f);" << vec6;

	Vector3f vec7(1.0f);
	cout << "\n\tVector3f vec7(1.0f);" << vec7;
}

void VectorTest::TestConstants()
{
	cout << "\n\tVector4f::zero()" << Vector4f::zero();
	cout << "\n\tVector4d::zero()" << Vector4d::zero();
	cout << "\n\tVector3f::zero()" << Vector3f::zero();
	cout << "\n\tVector3d::zero()" << Vector3d::zero();
	cout << "\n\tVector2f::zero()" << Vector2f::zero();
	cout << "\n\tVector2d::zero()" << Vector2d::zero();

	cout << "\n\tVector3f::forward();" << Vector3f::forward();
	cout << "\n\tVector3d::forward();" << Vector3d::forward();

	cout << "\n\tVector3f::backward();" << Vector3f::backward();
	cout << "\n\tVector3d::backward();" << Vector3d::backward();

	cout << "\n\tVector3f::left();" << Vector3f::left();
	cout << "\n\tVector3d::left();" << Vector3d::left();
	cout << "\n\tVector2f::left();" << Vector2f::left();
	cout << "\n\tVector2d::left();" << Vector2d::left();

	cout << "\n\tVector3f::right();" << Vector3f::right();
	cout << "\n\tVector3d::right();" << Vector3d::right();
	cout << "\n\tVector2f::right();" << Vector2f::right();
	cout << "\n\tVector2d::right();" << Vector2d::right();

	cout << "\n\tVector3f::up();" << Vector3f::up();
	cout << "\n\tVector3d::up();" << Vector3d::up();
	cout << "\n\tVector2f::up();" << Vector2f::up();
	cout << "\n\tVector2d::up();" << Vector2d::up();

	cout << "\n\tVector3f::down();" << Vector3f::down();
	cout << "\n\tVector3d::down();" << Vector3d::down();
	cout << "\n\tVector2f::down();" << Vector2f::down();
	cout << "\n\tVector2d::down();" << Vector2d::down();
}
void VectorTest::TestAssignments()
{
	const Vector3d vec3d(Vector3d::zero());
	cout << vec3d;
}
void VectorTest::TestScale()
{
	Vector3f vecA = Vector3f::left();
	cout << "vecA = Vector3f::left();" << vecA;

	vecA *= 2.0f;
	cout << "vecA *= 2.0f;" << vecA;

	vecA.Scale(0.5);
	cout << "vecA.Scale(0.5);" << vecA;

	vecA = vecA * 2;
	cout << "vecA = vecA * 2;" << vecA;

	vecA = 2 * vecA;
	cout << "vecA = 2 * vecA;" << vecA;

	vecA /= 2.0f;
	cout << "vecA /= 2.0f;" << vecA;

	vecA = vecA / 2;
	cout << "vecA = vecA / 2;" << vecA;
}
void VectorTest::TestAddition()
{
	Vector3f vecA = Vector3f::left();
	cout << "vecA = Vector3f::left;" << endl;
	cout << vecA << endl << endl;

	vecA += Vector3f::right();
	cout << "vecA += Vector3f::right;" << endl;
	cout << vecA << endl << endl;

	vecA = Vector3f::up() + Vector3f::right() + Vector3f::forward();
	cout << "vecA = Vector3f::up + Vector3f::right + Vector3f::forward;" << endl;
	cout << vecA << endl << endl;

	cout << "Vector2d(5, 5) - (const Vector2d( 1, 2 )" << endl;
	cout << (Vector2d(5.0, 5.0) - (const Vector2d(1.0, 2.0))) << endl << endl;
	cout << "(const Vector2d( 1, 2 )) - Vector2d(5, 5) - Vector2d(-1, 6)" << endl;
	cout << ((const Vector2d(1.0, 2.0)) - Vector2d(5.0, 5.0) - Vector2d(-1.0, 6.0)) << endl << endl;
}

void VectorTest::TestDot()
{
	float result = Vector3f(1.0f, 3.0f, 0.0f).Dot(Vector3f(1.0f, 1.0f, 0.0f));
	cout << "Vector3f(1.0f, 3.0f, 0.0f).Dot(Vector3f(1.0f, 1.0f, 0.0f)): " << endl;
	cout << result << endl << endl;
}
void VectorTest::TestCross()
{
	cout << "(1.0f, 0.0f, 0.0f).Cross(0.0f, 1.0f, 0.0f)" << endl;
	cout << Vector3f(1.0f, 0.0f, 0.0f).Cross(Vector3f(0.0f, 1.0f, 0.0f)) << endl << endl;
	cout << "(0.0f, 1.0f, 0.0f).Cross(1.0f, 0.0f, 0.0f)" << endl;
	cout << Vector3f(0.0f, 1.0f, 0.0f).Cross(Vector3f(1.0f, 0.0f, 0.0f)) << endl << endl;
}

void VectorTest::TestMagnitude()
{
	Vector3f vec1(1.0f, 3.0f, 0.0f);
	cout << "vec1(1.0f, 3.0f, 0.0f)" << endl << vec1 << endl;
	cout << "\tMagnitude: " << vec1.Magnitude() << endl;

	const Vector3f vec2(1.0f, 3.0f, 0.0f);
	cout << "const vec2(1.0f, 3.0f, 0.0f)" << endl << vec2 << endl;
	cout << "\tMagnitude: " << vec2.Magnitude() << endl << endl;
}

void VectorTest::TestSqrMagnitude()
{
	Vector3f vec1(1.0f, 3.0f, 0.0f);
	cout << "vec1(1.0f, 3.0f, 0.0f)" << endl << vec1 << endl;
	cout << "\tMagnitudeSq: " << vec1.MagnitudeSq() << endl << endl;

	const Vector3f vec2(1.0f, 3.0f, 0.0f);
	cout << "const vec2(1.0f, 3.0f, 0.0f)" << endl << vec2 << endl;
	cout << "\tMagnitudeSq: " << vec2.MagnitudeSq() << endl << endl;
}
void VectorTest::TestNormalized()
{
	Vector3f vec1(1.0f, 3.0f, 0.0f);
	cout << "vec1(1.0f, 3.0f, 0.0f)" << endl << vec1 << endl;
	cout << "vec1(1.0f, 3.0f, 0.0f).Normalized()" << endl << vec1.Normalized() << endl << endl;

	const Vector3f vec2(1.0f, 3.0f, 0.0f);
	cout << "const vec2(1.0f, 3.0f, 0.0f)" << endl << vec2 << endl << endl;
	cout << "const vec2(1.0f, 3.0f, 0.0f).Normalized()" << endl << vec2.Normalized() << endl << endl;

	const Vector3f vec3(vec2.Normalized());
	cout << "const vec3(vec2.Normalized())" << endl << vec3 << endl << endl;
}
void VectorTest::TestNormalize()
{
	Vector3f vec1(1.0f, 3.0f, 0.0f);
	cout << "vec1(1.0f, 3.0f, 0.0f)" << endl << vec1 << endl << endl;
	vec1.Normalize();
	cout << ".Normalize(); vec1(1.0f, 3.0f, 0.0f)" << endl << vec1 << endl << endl;
}

END_NAMESPACE(Seizyur, Tests, Math)
