#include "../MatrixTest.h"

using namespace std;

using Seizyur::Math::Matrix;

BEGIN_NAMESPACE(Seizyur, Tests, Math)

bool MatrixTest::Run()
{
	PrintHeading("MatrixTest::Run()");
	MatrixTest tests;

	tests.TestConstruction();
	tests.TestAssignment();
	tests.TestTranspose();
	tests.TestAddition();
	tests.TestScalarMul();
	tests.TestScalarDiv();
	tests.TestMatrixMul();
	tests.TestMatrixMul2();
	tests.TestMatrixMul3();
	tests.TestInverse();
	tests.TestDoolittle();
	tests.TestSolver();
	tests.TestSubMatrix();

	return true;
}

bool MatrixTest::TestConstruction()
{
	PrintHeading("MatrixTest::TestConstruction()");

	typedef Matrix<2, 2, int> Mat2x2i;
	cout << "Mat2x2i() - c03 default initialization" << (Mat2x2i());
	cout << "Mat2x2i{} - c11 default initialization" << (Mat2x2i{});
	cout << "Mat2x2i(1,2) - c03 partial initialization" << (Mat2x2i(1, 2));
	cout << "Mat2x2i{1,2} - c11 partial initialization" << (Mat2x2i{ 1, 2 });
	cout << "Mat2x2i(1,2,3,4) - c03 full initialization" << (Mat2x2i(1, 2, 3, 4));
	cout << "Mat2x2i{1,2,3,4} - c11 full initialization" << (Mat2x2i{ 1, 2, 3, 4 });

	typedef const Matrix<2, 2, int> Mat2x2ic;
	cout << "const Mat2x2ic() - c03 default initialization" << (Mat2x2ic());
	cout << "const Mat2x2ic{} - c11 default initialization" << (Mat2x2ic{});
	cout << "const Mat2x2ic(1,2) - c03 partial initialization" << (Mat2x2ic(1, 2));
	cout << "const Mat2x2ic{1,2} - c11 partial initialization" << (Mat2x2ic{ 1, 2 });
	cout << "const Mat2x2ic(1,2,3,4) - c03 full initialization" << (Mat2x2ic(1, 2, 3, 4));
	cout << "const Mat2x2ic{1,2,3,4} - c11 full initialization" << (Mat2x2ic{ 1, 2, 3, 4 });

	typedef const Matrix<3, 2, float> Mat3x2fc;
	cout << "\nconst Mat3x2fc() - c03 default initialization";
	cout << (Mat3x2fc());
	cout << "\nconst Mat3x2fc{} - c11 default initialization";
	cout << (Mat3x2fc{});
	cout << "\nconst Mat3x2fc(1.f,2.f) - c03 partial initialization";
	cout << (Mat3x2fc(1.f, 2.f));
	cout << "\nconst Mat3x2fc{1.f,2.f} - c11 partial initialization";
	cout << (Mat3x2fc{ 1.f, 2.f });
	cout << "\nconst Mat3x2fc(1.f, 2.f, 3.f, 4.f, 5.f, 6.f) - c03 full initialization";
	cout << (Mat3x2fc(1.f, 2.f, 3.f, 4.f, 5.f, 6.f));
	cout << "\nconst Mat3x2fc{1.f, 2.f, 3.f, 4.f, 5.f, 6.f} - c11 full initialization";
	cout << (Mat3x2fc{ 1.f, 2.f, 3.f, 4.f, 5.f, 6.f });

	typedef Matrix<2, 3, float> Mat2x3f;
	cout << "\nMat2x3f() - c03 default initialization";
	cout << (Mat2x3f());
	cout << "\nMat2x3f{} - c11 default initialization";
	cout << (Mat2x3f{});
	cout << "\nMat2x3f(1.f,2.f) - c03 partial initialization";
	cout << (Mat2x3f(1.f, 2.f));
	cout << "\nMat2x3f{1.f,2.f} - c11 partial initialization";
	cout << (Mat2x3f{ 1.f, 2.f });
	cout << "\nMat2x3f(1.f, 2.f, 3.f, 4.f, 5.f, 6.f) - c03 full initialization";
	cout << (Mat2x3f(1.f, 2.f, 3.f, 4.f, 5.f, 6.f));
	cout << "\nMat2x3f{1.f, 2.f, 3.f, 4.f, 5.f, 6.f} - c11 full initialization";
	cout << (Mat2x3f{ 1.f, 2.f, 3.f, 4.f, 5.f, 6.f });

	return true;
}

bool MatrixTest::TestAssignment()
{
	PrintHeading("MatrixTest::TestAssignment()");
	return true;
}

bool MatrixTest::TestAddition()
{
	PrintHeading("MatrixTest::TestAddition()");
	return true;
}

bool MatrixTest::TestTranspose()
{
	PrintHeading("MatrixTest::TestTranspose()");
	Matrix<2, 3, float> mat1{ 1.f, 2.f, 3.f, 4.f, 5.f, 6.f };
	cout << "Original:" << mat1;
	auto mat1t = mat1.GetTranspose();
	cout << "\nTranspose:" << mat1t;

	Matrix<1, 6, float> mat2{ 1.f, 2.f, 3.f, 4.f, 5.f, 6.f };
	cout << "Original:" << mat2;
	auto mat2t = mat2.GetTranspose();
	cout << "\nTranspose:" << mat2t;
	return true;
}

END_NAMESPACE(Seizyur, Tests, Math)
