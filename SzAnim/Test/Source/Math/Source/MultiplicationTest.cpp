#include "../MatrixTest.h"

using namespace std;

using Seizyur::Math::Matrix;

BEGIN_NAMESPACE(Seizyur, Tests, Math)

bool MatrixTest::TestScalarMul()
{
	PrintHeading("Scalar Multiplication");
	Matrix<2, 3, float> A{ 1.f, 2.f, 3.f, 4.f, 5.f, 6.f };
	auto B = A;
	float scalar = 2.0f;

	B = A * scalar;

	// 	cout << "A * scalar:" << (A * scalar);
	// 	cout << "scalar * A:" << (scalar * A);
	// 
	// 	B = A * scalar;
	// 	cout << "B = A * scalar:" << B;
	// 
	// 	B = scalar * A;
	// 	cout << "B = scalar * A:" << B;
	// 
	// 	B = A;
	// 	B = B * scalar;
	// 	cout << "B = B * scalar:" << B;
	// 
	// 	B = A;
	// 	B = scalar * B;
	// 	cout << "B = scalar * B:" << B;

	return true;
}

bool MatrixTest::TestScalarDiv()
{
	PrintHeading("Scalar Division");
	Matrix<2, 3, float> A{ 1.f, 2.f, 3.f, 4.f, 5.f, 6.f };
	auto B = A;
	float scalar = 2.0f;

	cout << "(A / scalar)" << (A / scalar);

	B = A / scalar;
	cout << "B = A / scalar:" << B;

	B = A;
	B = B / scalar;
	cout << "B = B / scalar:" << B;

	B = A;
	B /= scalar;
	cout << "B /= scalar:" << B;

	return true;
}

bool MatrixTest::TestMatrixMul()
{
	PrintHeading("A = B * C");
	Matrix<2, 3, float> A{ 1.f, 2.f, 3.f, 4.f, 5.f, 6.f };
	Matrix<3, 5, float> B{ 1.f, 2.f, 3.f, 4.f, 5.f, 6.f };
	auto C = A * B;

	cout << "A" << A;
	cout << "B" << B;
	cout << "C = A * B" << C;

	return true;
}

bool MatrixTest::TestMatrixMul2()
{
	PrintHeading("A *= B");
	Matrix<3, 3, float> lhs{ 1.f, 2.f, 3.f, 4.f, 3.f, 3.f };
	Matrix<3, 3, float> rhs{ 3.f, 2.f, 3.f, 4.f, 5.f, 6.f };

	cout << "lhs" << lhs;
	cout << "rhs" << rhs;

	cout << "(lhs * rhs)" << (lhs * rhs);

	cout << "(rhs * lhs)" << (rhs * lhs);

	lhs *= rhs;
	cout << "lhs *= rhs" << lhs;

	return true;
}

bool MatrixTest::TestMatrixMul3()
{
	PrintHeading("MatrixTest::TestMatrixMul2() A = B * C * D");
	Matrix<3, 3, float> A{ 1.f, 2.f, 3.f, 2.f, 3.f, 1.f, 3.f, 1.f, 2.f };
	Matrix<3, 3, float> B{ 3.f, 2.f, 3.f, 4.f, 3.f, 2.f, 3.f, 2.f, 3.f };
	Matrix<3, 3, float> C{ 5.f, 1.f, 4.f, 2.f, 6.f, 1.f, 1.f, 4.f, 2.f };
	Matrix<3, 3, float> D{ 1.f, 2.f, 0.f, 2.f, 2.f, 0.f, 3.f, 3.f, 2.f };
	Matrix<3, 3, float> E{ 6.f, 2.f, 6.f, 5.f, 3.f, 1.f, 4.f, 5.f, 2.f };

	cout << "Result: " << (A * B * C * D * E);
	cout << "Result: " << ((A * B) * C * (D * E));
	cout << "Result: " << (A * (B * C * D) * E);
	cout << "Result: " << (A * (B * C) * D * E);
	cout << "Result: " << ((A * B * C) * (D * E));

	return true;
}

END_NAMESPACE(Seizyur, Tests, Math)
