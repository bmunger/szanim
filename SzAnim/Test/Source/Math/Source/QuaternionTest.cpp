#include "../QuaternionTest.h"

using namespace std;
using namespace Seizyur::Math;
using namespace Seizyur::Math::Geometry;

using Seizyur::Core::traits;

BEGIN_NAMESPACE(Seizyur, Tests, Math)

bool QuaternionTest::Run()
{
	QuaternionTest tests;

	tests.TestConstructors();

// 	tests.TestInitialization();
// 	tests.TestArithmeticOperators();
// 	tests.TestConversionOperators();
// 	tests.TestMethods();
// 	tests.TestStaticFunctions();

	return true;
}
void QuaternionTest::TestConstructors()
{
	PrintHeading("Constructors", '-', 2);

	TestConstructor<Quaternionf, float>("float");
	TestConstructor<const Quaternionf, float>("const float");
	TestConstructor<Quaterniond, double>("double");
	TestConstructor<const Quaterniond, double>("const double");
	TestConstructor<Quaternion<int>, int>("int");
	TestConstructor<const Quaternion<int>, int>("const int");
}
template<typename _QUATTYPE, typename _SCALARTYPE>
void QuaternionTest::TestConstructor(const char *logString)
{
	PrintHeading(logString, ' ', 1);

	typedef _QUATTYPE QuatType;
	typedef _SCALARTYPE ScalarType;

	ScalarType val = traits<ScalarType>::one();

	QuatType quat1(val, val, val, val);
	SZ_DEBUG_VAR(quat1);
	val += val;
	SZ_DEBUG_VAR(QuatType(val, val, val, val));
	val += val;
	SZ_DEBUG_VAR(QuatType(Vector<3, ScalarType>(val, val, val)));
	val += val;
	SZ_DEBUG_VAR(QuatType(Vector<3, ScalarType>(val, val, val), val));
	val += val;
	SZ_DEBUG_VAR(QuatType(Vector<4, ScalarType>(val, val, val, val)));

	val += val;
	QuatType quat2(const Vector<3, ScalarType>(val, val + val, val));
	val += val;
	QuatType quat3(const Vector<3, ScalarType>(val, val, val), val);
	val += val;
	QuatType quat4(const Vector<4, ScalarType>(val, val, val, val));

	SZ_DEBUG_VAR(quat2);
	SZ_DEBUG_VAR(quat3);
	SZ_DEBUG_VAR(quat4);
}

void QuaternionTest::TestInitialization()
{

}
void QuaternionTest::TestConversionOperators() {}
void QuaternionTest::TestMethods() {}
void QuaternionTest::TestStaticFunctions() {}

void QuaternionTest::TestArithmeticOperators()
{
	PrintHeading("Arithmetic Operators", '-', 2);

	TestArithmeticOperator<Quaternionf, float>("float");
	TestArithmeticOperator<const Quaternionf, float>("const float");
	TestArithmeticOperator<Quaterniond, double>("double");
	TestArithmeticOperator<const Quaterniond, double>("const double");
	TestArithmeticOperator<Quaternion<int>, int>("int");
	TestArithmeticOperator<const Quaternion<int>, int>("const int");
}

template<typename _QUATTYPE, typename _SCALARTYPE>
void QuaternionTest::TestArithmeticOperator(const char *logString)
{
	PrintHeading(logString, ' ', 1);

	typedef _SCALARTYPE ScalarType;
	typedef _QUATTYPE QuatType;
	typedef Vector<3, ScalarType> Vec3Type;
	typedef Vector<4, ScalarType> Vec4Type;

	ScalarType val = traits<ScalarType>::one();

	QuatType quat1(val, val, val, val);
	SZ_DEBUG_VAR(quat1);
	val += val;
	SZ_DEBUG_VAR(QuatType(val, val, val, val));
	val += val;
	SZ_DEBUG_VAR(QuatType(Vector<3, ScalarType>(val, val, val)));
	val += val;
	SZ_DEBUG_VAR(QuatType(Vector<3, ScalarType>(val, val, val), val));
	val += val;
	SZ_DEBUG_VAR(QuatType(Vector<4, ScalarType>(val, val, val, val)));

	val += val;
	QuatType quat2(const Vector<3, ScalarType>(val, val + val, val));
	val += val;
	QuatType quat3(const Vector<3, ScalarType>(val, val, val), val);
	val += val;
	QuatType quat4(const Vector<4, ScalarType>(val, val, val, val));

	SZ_DEBUG_VAR(quat2);
	SZ_DEBUG_VAR(quat3);
	SZ_DEBUG_VAR(quat4);
}


END_NAMESPACE(Seizyur, Tests, Math)
