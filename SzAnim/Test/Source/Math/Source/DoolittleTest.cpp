#include "../MatrixTest.h"

BEGIN_NAMESPACE(Seizyur, Tests, Math)

using namespace std;

using Seizyur::Math::Matrix;

bool MatrixTest::TestDoolittle()
{
	PrintHeading("MatrixTest::TestDoolittle()");

	Matrix<3, 3, float> mat{ 1.0f, 2.0f, 1.0f, 2.0f, 2.0f, 2.0f, 1.0f, 2.0f, 3.0f };
	Matrix<3, 3, float> lower;
	Matrix<3, 3, float> upper;

	mat.Doolittle(lower, upper);

	cout << "mat:" << mat;
	cout << "lower:" << lower;
	cout << "upper:" << upper;
	cout << "Restored:" << (lower*upper);

	Matrix<3, 3, float> mat2{ 5.0f, 3.0f, 10.0f, 2.0f, 32.0f, 29.0f, 1.0f, 42.0f, 3.0f };
	mat2.Doolittle(lower, upper);

	cout << "mat2" << mat2;
	cout << "lower" << lower;
	cout << "upper" << upper;
	cout << "Restored" << (lower*upper);

	return true;
}

END_NAMESPACE(Seizyur, Tests, Math)
