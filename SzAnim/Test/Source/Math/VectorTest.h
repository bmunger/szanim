#ifndef SEIZYUR_TEST_H_VECTOR
#define SEIZYUR_TEST_H_VECTOR

#include "../TestBase.h"

BEGIN_NAMESPACE(Seizyur, Tests, Math)

class VectorTest : public TestBase
{
public:
	static bool Run();

private:
	void TestInitialization();
	void TestConstants();
	void TestAssignments();
	void TestAddition();
	void TestScale();
	void TestDot();
	void TestCross();
	void TestMagnitude();
	void TestSqrMagnitude();
	void TestNormalized();
	void TestNormalize();
};

END_NAMESPACE(Seizyur, Tests, Math)

#endif SEIZYUR_TEST_H_VECTOR
