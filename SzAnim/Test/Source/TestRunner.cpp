#include "TestRunner.h"

#include "TestBase.h"
#include "Core/Array2dTest.h"
#include "Math/MatrixTest.h"
#include "Math/VectorTest.h"
#include "Math/QuaternionTest.h"
#include "Math/TransformTest.h"

#include <Seizyur.h>

BEGIN_NAMESPACE(Seizyur, Tests)

using namespace std;
using namespace Math;

enum class AvailableTests : int
{
	FIRST = 0,
	Array2d,
	Matrix,
	Vector,
	Quaternion,
	Transform,
	Quit,
	NUM_OPTIONS,
};

void PrintOption(char* name, AvailableTests option)
{
	cout << endl << setfill(' ') << setw(12) << std::right << name << ": " << static_cast<int>(option);
}
void PrintOptions()
{
	PrintOption("Array2d", AvailableTests::Array2d);
	PrintOption("Matrix", AvailableTests::Matrix);
	PrintOption("Vector", AvailableTests::Vector);
	PrintOption("Quaternion", AvailableTests::Quaternion);
	PrintOption("Transform", AvailableTests::Transform);
	PrintOption("Quit", AvailableTests::Quit);
}

const AvailableTests RequestTestType()
{
	while(1)
	{
		int inType = -1;
		PrintSeparator('=', 0);
		PrintOptions();

		cout << endl << "\nEnter id for test to run: ";
		cin >> inType;

		auto inEnumType = static_cast<AvailableTests>(inType);
		if(cin && inEnumType > AvailableTests::FIRST && inEnumType < AvailableTests::NUM_OPTIONS)
			return inEnumType;

		cout << endl << "Whoops. Try again." << endl;
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
}

void RunTest(const char *testName, bool(*test)())
{
	PrintHeading(testName, '=');
	if(test != nullptr)
		test();
	PrintSeparator('-', 1);
}

void TestRunner::Run()
{
	bool run = true;

	while(run)
	{
		switch(RequestTestType())
		{
		case AvailableTests::Array2d:
			RunTest("Array2d", &(Array2dTest::Run));
			break;
		case AvailableTests::Matrix:
			RunTest("Matrix", &(MatrixTest::Run));
			break;
		case AvailableTests::Vector:
			RunTest("Vector", &(VectorTest::Run));
			break;
		case AvailableTests::Quaternion:
			RunTest("Quaternion", &(QuaternionTest::Run));
			break;
		case AvailableTests::Transform:
			RunTest("Transform", &(TransformTest::Run));
			break;
		case AvailableTests::Quit:
			run = false;
			break;
		default:
			break;
		}
	}
	cin.get();
}
END_NAMESPACE(Seizyur, Tests)