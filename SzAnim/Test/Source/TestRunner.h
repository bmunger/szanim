#ifndef SEIZYUR_TEST_H_TESTRUNNER
#define SEIZYUR_TEST_H_TESTRUNNER

#include <Seizyur.h>

BEGIN_NAMESPACE(Seizyur, Tests)
class TestRunner
{
public:
	static void Run();
};
END_NAMESPACE(Seizyur, Tests)

#endif // SEIZYUR_TEST_H_TESTRUNNER
