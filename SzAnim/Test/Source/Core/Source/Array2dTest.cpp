#include "../Array2dTest.h"

BEGIN_NAMESPACE(Seizyur, Tests, Math)

using namespace std;

using Seizyur::Core::Array2d;

bool Array2dTest::Run()
{
	Array2dTest tests;
	tests.TestArray2dAllocations();
	tests.TestConstArray2dAllocations();

	return true;
}

void Array2dTest::TestArray2dAllocations()
{
	{
		Array2d<1, 1, float> asdf{2.345f};
		asdf(0, 0) = 4.0f;
		Print("1x1", asdf);
		cout << endl;
	}
	{
		Array2d<2, 2, float> asdf;
		asdf(0, 0) = 1.0f;
		asdf(0, 1) = 2.2354f;
		asdf(1, 0) = 3.34f;
		asdf(1, 1) = 4.5643674362f;
		Print("2x2", asdf);
		cout << endl;
	}
	{
		Array2d<1, 5, float> asdf;
		asdf[0] = 1.0f;
		asdf[1] = 2.0f;
		asdf[2] = 3.0f;
		asdf[3] = 4.0f;
		asdf[4] = 5.0f;
		Print("1x5", asdf);
		cout << endl;
	}
	{
		Array2d<5, 1, float> asdf;
		asdf[0] = 1.2f;
		asdf[1] = 2.0f;
		asdf[2] = 3.23542354f;
		asdf[3] = 4.0f;
		asdf[4] = 5.0f;
		Print("5x1", asdf);
		cout << endl;
	}
}

void Array2dTest::TestConstArray2dAllocations()
{
	// 	cout << endl;
	// 	{
	// 		const Array2d<1, 1, float> asdf;
	// 		asdf.Print();
	// 	}
	// 	cout << endl;
	// 	{
	// 		const Array2d<2, 2, float> asdf;
	// 		asdf.Print();
	// 	}
	// 	cout << endl;
	// 	{
	// 		const Array2d<1, 5, float> asdf;
	// 		asdf.Print();
	// 	}
	// 	cout << endl;
}
END_NAMESPACE(Seizyur, Tests, Math)
