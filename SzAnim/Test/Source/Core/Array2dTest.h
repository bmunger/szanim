#ifndef SEIZYUR_TEST_H_ARRAY2DTEST
#define SEIZYUR_TEST_H_ARRAY2DTEST

#include "../TestBase.h"

BEGIN_NAMESPACE(Seizyur, Tests, Math)

class Array2dTest : public TestBase
{
public:
	static bool Run();

private:
	void TestArray2dAllocations();
	void TestConstArray2dAllocations();
};

END_NAMESPACE(Seizyur, Tests, Math)

#endif // SEIZYUR_TEST_H_ARRAY2DTEST
