#ifndef SEIZYUR_MATH_H_SPLINE
#define SEIZYUR_MATH_H_SPLINE

BEGIN_NAMESPACE(Seizyur, Math)

template<typename _POINTTYPE>
class Spline
{
	typedef _POINTTYPE PointType;
public:

	int KnotCount() const { return _knots.length; }
	int ControlCount() const { return _ctrls.length; }

	void Degree(int degree) { _degree = degree; }
	int Degree() const { return _degree; }

private:
	std::vector<PointType> _knots;
	std::vector<PointType> _ctrls;
	int _degree;
};

END_NAMESPACE(Seizyur, Math)

#endif // SEIZYUR_MATH_H_SPLINE