#ifndef SEIZYUR_MATH_H_FUNCTIONS
#define SEIZYUR_MATH_H_FUNCTIONS

BEGIN_NAMESPACE(Seizyur, Math)

template<typename Scalar>
Scalar clamp(const Scalar &val, const Scalar &minVal, const Scalar &maxVal)
{
	return (val < minVal) ? minVal : ((val > maxVal) ? maxVal : val);
}

template<typename Scalar>
Scalar clamp01(const Scalar &val)
{
	return clamp(val, traits<Scalar>::zero(), traits<Scalar>::one());
}

template<typename Scalar>
Scalar lerp(const Scalar &from, const Scalar &to, float alpha)
{
	return (to - from) * alpha + from;
}

template<typename Scalar>
Scalar slerp(const Scalar &from, const Scalar &to, float alpha)
{
	return (to - from) * alpha + from;
}

END_NAMESPACE(Seizyur, Math)
#endif