#ifndef SEIZYUR_MATH_H_TRAITS
#define SEIZYUR_MATH_H_TRAITS

BEGIN_NAMESPACE(Seizyur, Core)

template<typename _T>
struct _traits< _T, typename std::enable_if<std::is_arithmetic<_T>::value>::type >
{
	using type = std::remove_cv_t<_T>;

	static inline type zero() { return 0; }
	static inline type one() { return 1; }
	static inline type eps() { return std::numeric_limits<_T>::epsilon(); }
	static inline type min() { return std::numeric_limits<_T>::lowest(); }
	static inline type max() { return std::numeric_limits<_T>::max(); }
};

template<int _ROWS, int _COLUMNS, typename _SCALARTYPE>
struct _traits< Math::Matrix<_ROWS, _COLUMNS, _SCALARTYPE> >
{
	using scalar_t = std::remove_cv_t<_SCALARTYPE>;
	using type = Math::Matrix<_ROWS, _COLUMNS, scalar_t>;

	enum
	{
		rows = _ROWS,
		columns = _COLUMNS,
		width = _COLUMNS,
		height = _ROWS
	};

	using transpose_t = Math::Matrix < _COLUMNS, _ROWS, scalar_t>;
	using row_t = Math::Matrix < 1, _COLUMNS, scalar_t>;
	using column_t = Math::Matrix < _ROWS, 1, scalar_t>;
	using inverse_t = Math::Matrix < _ROWS < _COLUMNS ? _ROWS : _COLUMNS, _ROWS < _COLUMNS ? _ROWS : _COLUMNS, scalar_t>;
	using storage_t = Array2d<_COLUMNS, _ROWS, scalar_t>;
};

template<int _SIZE, typename _SCALARTYPE>
struct _traits< Math::Geometry::Vector<_SIZE, _SCALARTYPE> >
{
	using scalar_t = std::remove_cv_t<_SCALARTYPE>;
	using type = Math::Geometry::Vector<_SIZE, scalar_t>;

	enum { Size = _SIZE };
};

template<typename _SCALARTYPE>
struct _traits< Math::Geometry::Quaternion<_SCALARTYPE> >
{
	using scalar_t = std::remove_cv_t<_SCALARTYPE>;
	using type = Math::Geometry::Quaternion<scalar_t>;
};

END_NAMESPACE(Seizyur, Core)
#endif // SEIZYUR_MATH_H_TRAITS
