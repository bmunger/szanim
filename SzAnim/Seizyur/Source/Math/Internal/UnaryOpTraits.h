#ifndef SEIZYUR_CORE_H_UNARYOPTRAITS
#define SEIZYUR_CORE_H_UNARYOPTRAITS

BEGIN_NAMESPACE(Seizyur, Math, Internal)

template<typename _T1, typename _RESULT>
struct _unary_traits
{
	using arg_t = _T1;
	using result_t = _RESULT;
};

template<typename _T1, typename _RESULT>
struct unary_traits
	: _unary_traits< std::remove_cv_t<_T1>, std::remove_cv_t<_RESULT> >
{ };

END_NAMESPACE(Seizyur, Math, Internal)
#endif // SEIZYUR_CORE_H_UNARYOPTRAITS
