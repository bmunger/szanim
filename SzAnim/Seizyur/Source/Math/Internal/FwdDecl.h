#ifndef SEIZYUR_MATH_H_FWDDECL
#define SEIZYUR_MATH_H_FWDDECL

BEGIN_NAMESPACE(Seizyur, Math)
template<int _ROWS, int _COLUMNS, typename _SCALARTYPE> class Matrix;
template<typename _DERIVED> class MatrixBase;
END_NAMESPACE(Seizyur, Math)

#endif // SEIZYUR_MATH_H_FWDDECL
