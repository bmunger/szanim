#ifndef SEIZYUR_CORE_H_BINARYOPTRAITS
#define SEIZYUR_CORE_H_BINARYOPTRAITS

BEGIN_NAMESPACE(Seizyur, Math, Internal)

template<typename _LHS, typename _RHS, typename _RESULT>
struct _binary_traits
{
	using lhs_t = _LHS;
	using rhs_t = _RHS;
	using result_t = _RESULT;
};

template<typename _LHS, typename _RHS, typename _RESULT>
using binary_traits = _binary_traits< std::remove_cv_t<_LHS>, std::remove_cv_t<_RHS>, std::remove_cv_t<_RESULT> >;

template<typename _LHS, typename _RHS, typename _ENABLE = void>
struct _matrix_outer_product_traits;

template<typename _LHS, typename _RHS>
struct _matrix_outer_product_traits< _LHS, _RHS, std::enable_if_t<is_matrix_v<_LHS> && is_matrix_v<_RHS> && is_multipliable_v<_LHS, _RHS>> >
	: binary_traits<
	Matrix< _LHS::rows, _LHS::columns, std::remove_cv_t<typename _LHS::scalar_t> >,
	Matrix< _RHS::rows, _RHS::columns, std::remove_cv_t<typename _RHS::scalar_t> >,
	Matrix< _LHS::rows, _RHS::columns, std::remove_cv_t<typename _LHS::scalar_t> > >
{ };

template<typename _LHS, typename _RHS>
struct matrix_outer_product_traits;

template<typename _LHS, typename _RHS>
struct matrix_outer_product_traits
	: _matrix_outer_product_traits< std::remove_cv_t<_LHS>, std::remove_cv_t<_RHS> >
{ };

END_NAMESPACE(Seizyur, Math, Internal)
#endif // SEIZYUR_CORE_H_BINARYOPTRAITS
