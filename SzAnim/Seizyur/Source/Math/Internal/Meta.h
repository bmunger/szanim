#ifndef SEIZYUR_CORE_H_META
#define SEIZYUR_CORE_H_META

BEGIN_NAMESPACE(Seizyur, Math, Internal)

template<typename _T>
struct _is_matrix
	: std::false_type
{ };

template<int _ROWS, int _COLUMNS, typename _SCALAR>
struct _is_matrix< Matrix<_ROWS, _COLUMNS, _SCALAR> >
	: std::true_type
{ };

template<typename _DERIVED>
struct _is_matrix< MatrixBase<_DERIVED> >
	: std::true_type
{ };

template<typename _T>
using is_matrix = _is_matrix< std::remove_cv_t<_T> >;

template<typename _T>
constexpr bool is_matrix_v = is_matrix<_T>::value;

template<typename _LHS, typename _RHS, typename _ENABLE = void>
struct _is_multipliable
	: std::false_type
{ };

template<typename _LHS, typename _RHS>
struct _is_multipliable< _LHS, _RHS, std::enable_if_t<std::is_arithmetic<_LHS>::value && std::is_arithmetic<_RHS>::value> >
	: std::bool_constant< std::is_convertible<_RHS, _LHS>::value >
{ };

template<typename _LHS, typename _RHS>
struct _is_multipliable< _LHS, _RHS, std::enable_if_t<is_matrix<_LHS>::value && is_matrix<_RHS>::value> >
	: std::bool_constant< Core::traits<_LHS>::columns == Core::traits<_RHS>::rows >
{ };

template<typename _LHS, typename _RHS>
using is_multipliable = _is_multipliable< std::remove_cv_t<_LHS>, std::remove_cv_t<_RHS> >;

template<typename _LHS, typename _RHS>
constexpr bool is_multipliable_v = _is_multipliable< std::remove_cv_t<_LHS>, std::remove_cv_t<_RHS> >::value;

END_NAMESPACE(Seizyur, Math, Internal)
#endif // SEIZYUR_CORE_H_META
