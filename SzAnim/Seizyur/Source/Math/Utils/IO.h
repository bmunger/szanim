#ifndef SEIZYUR_UTILS_H_OUTSTREAM
#define SEIZYUR_UTILS_H_OUTSTREAM

BEGIN_NAMESPACE(Seizyur, Debug)

template<typename _DERIVED>
std::ostream& operator<<(std::ostream& out, const Math::MatrixBase<_DERIVED>& obj)
{
	typedef Core::traits<_DERIVED> MatTraits;

	out << std::setprecision(3) << std::setfill(' ') << std::right;

	out << endl;
	for(int yIdx = 0; yIdx < MatTraits::rows; yIdx++)
	{
		out << "[";
		for(int xIdx = 0; xIdx < MatTraits::columns; xIdx++)
		{
			out << std::setw(4) << obj(yIdx*MatTraits::columns + xIdx)
				<< ((xIdx == MatTraits::columns - 1) ? "]" : ", ");
		}
		if(yIdx < MatTraits::rows - 1)
			out << "\n";
	}
	return out;
}

template<typename _DERIVED>
std::ostream& operator<<(std::ostream& out, const Math::Geometry::VectorBase<_DERIVED>& obj)
{
	out << std::setprecision(3) << std::setfill(' ') << std::right << "\n[";

	for(int idx = 0; idx < Core::traits<_DERIVED>::Size; idx++)
		out << std::setw(4) << obj[idx] << ((idx == Core::traits<_DERIVED>::Size - 1) ? "]" : ", ");
	return out;
}

template<typename _SCALARTYPE>
std::ostream& operator<<(std::ostream& out, const Math::Geometry::Quaternion<_SCALARTYPE>& obj)
{
	out << std::setprecision(3) << std::setfill(' ') << std::right << "\n[";

	for(int idx = 0; idx < 4; idx++)
		out << std::setw(4) << obj[idx] << ((idx == 3) ? "]" : ", ");
	return out;
}

END_NAMESPACE(Seizyur, Debug)
#endif // SEIZYUR_UTILS_H_OUTSTREAM