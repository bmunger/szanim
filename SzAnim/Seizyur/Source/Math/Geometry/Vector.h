#ifndef SEIZYUR_MATH_H_VECTOR
#define SEIZYUR_MATH_H_VECTOR

BEGIN_NAMESPACE(Seizyur, Math, Geometry)

template<int _SIZE, typename _SCALARTYPE>
class Vector : public VectorBase < Vector<_SIZE, _SCALARTYPE> >
{
public:
	template<typename ..._PTYPE>
	Vector(_PTYPE... args) : VectorBase{ args... } {}
};

template<typename _SCALARTYPE>
class Vector<2, _SCALARTYPE> : public VectorBase < Vector<2, _SCALARTYPE> >
{
public:
	static vector_t left() { vector_t retVal; retVal.Set(scalarTraits::one(), scalarTraits::zero()); return retVal; }
	static vector_t right() { vector_t retVal; retVal.Set(-scalarTraits::one(), scalarTraits::zero()); return retVal; }
	static vector_t up() { vector_t retVal; retVal.Set(scalarTraits::zero(), scalarTraits::one()); return retVal; }
	static vector_t down() { vector_t retVal; retVal.Set(scalarTraits::zero(), -scalarTraits::one()); return retVal; }

	void Set(const scalar_t x, const scalar_t y)
	{
		_data(0) = x;
		_data(1) = y;
	}

	scalar_t &x() { return _data(0); }
	const scalar_t &x() const { return _data(0); }

	scalar_t &y() { return _data(1); }
	const scalar_t &y() const { return _data(1); }

	template<typename ..._PTYPE>
	Vector(_PTYPE... args) : VectorBase{ args... } {}
};

template<typename _SCALARTYPE>
class Vector<3, _SCALARTYPE> : public VectorBase < Vector<3, _SCALARTYPE> >
{
public:
	static vector_t left() { vector_t retVal; retVal.Set(scalarTraits::one(), scalarTraits::zero(), scalarTraits::zero()); return retVal; }
	static vector_t right() { vector_t retVal; retVal.Set(-scalarTraits::one(), scalarTraits::zero(), scalarTraits::zero()); return retVal; }
	static vector_t up() { vector_t retVal; retVal.Set(scalarTraits::zero(), scalarTraits::one(), scalarTraits::zero()); return retVal; }
	static vector_t down() { vector_t retVal; retVal.Set(scalarTraits::zero(), -scalarTraits::one(), scalarTraits::zero()); return retVal; }
	static vector_t forward() { vector_t retVal; retVal.Set(scalarTraits::zero(), scalarTraits::zero(), scalarTraits::one()); return retVal; }
	static vector_t backward() { vector_t retVal; retVal.Set(scalarTraits::zero(), scalarTraits::zero(), -scalarTraits::one()); return retVal; }

	scalar_t &x() { return _data(0); }
	const scalar_t &x() const { return _data(0); }

	scalar_t &y() { return _data(1); }
	const scalar_t &y() const { return _data(1); }

	scalar_t &z() { return _data(2); }
	const scalar_t &z() const { return _data(2); }

	void Set(const scalar_t x, const scalar_t y, const scalar_t z)
	{
		_data(0) = x;
		_data(1) = y;
		_data(2) = z;
	}

	template<typename ..._PTYPE>
	Vector(_PTYPE... args) : VectorBase{ args... } {}
};

template<typename _SCALARTYPE>
class Vector<4, _SCALARTYPE> : public VectorBase < Vector<4, _SCALARTYPE> >
{
public:
	scalar_t &x() { return _data(0); }
	const scalar_t &x() const { return _data(0); }

	scalar_t &y() { return _data(1); }
	const scalar_t &y() const { return _data(1); }

	scalar_t &z() { return _data(2); }
	const scalar_t &z() const { return _data(2); }

	scalar_t &w() { return _data(3); }
	const scalar_t &w() const { return _data(3); }

	void Set(const scalar_t x, const scalar_t y, const scalar_t z, const scalar_t w)
	{
		_data(0) = x;
		_data(1) = y;
		_data(2) = z;
		_data(3) = w;
	}

	template<typename ..._PTYPE>
	Vector(_PTYPE... args) : VectorBase{ args... } {}
};

using Vector4f = Vector <4, float>;
using Vector4d = Vector <4, double>;

using Vector3f = Vector <3, float>;
using Vector3d = Vector <3, double>;

using Vector2f = Vector <2, float>;
using Vector2d = Vector <2, double>;
END_NAMESPACE(Seizyur, Math, Geometry)
#endif // SEIZYUR_MATH_H_VECTOR
