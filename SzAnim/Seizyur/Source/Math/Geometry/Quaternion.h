#ifndef SEIZYUR_MATH_H_QUATERNION
#define SEIZYUR_MATH_H_QUATERNION

BEGIN_NAMESPACE(Seizyur, Math, Geometry)

template<typename _SCALARTYPE>
class Quaternion
{
	typedef typename Quaternion<_SCALARTYPE> quaternion_t;
	typedef _SCALARTYPE scalar_t;
	typedef Vector<3, scalar_t> vector3_t;
	typedef Vector<4, scalar_t> vector4_t;
	typedef Matrix<3, 3, scalar_t> matrix_t;

	typedef Core::traits<_SCALARTYPE> ScalarTraits;

public:
	static quaternion_t identity() { return{ }.SetIdentity(); }
	static quaternion_t zero() { return{ }; }

	static quaternion_t LookAt(const vector3_t &forward, const vector3_t &up) { return identity(); }

	static bool IsEqual(const quaternion_t &lhs, const quaternion_t &rhs) { return lhs.IsEqual(rhs); }

	static quaternion_t Lerp(const quaternion_t &from, const quaternion_t &to, const float alpha)
	{
		return
		{
			lerp(from.x(), to.x(), alpha),
			lerp(from.y(), to.y(), alpha),
			lerp(from.z(), to.z(), alpha),
			lerp(from.w(), to.w(), alpha)
		};
	}
	static quaternion_t Slerp(const quaternion_t &from, const quaternion_t &to, const float alpha)
	{
		return
		{
			slerp(from.x(), to.x(), alpha),
			slerp(from.y(), to.y(), alpha),
			slerp(from.z(), to.z(), alpha),
			slerp(from.w(), to.w(), alpha)
		};
	}

	Quaternion() { SetZero(); }
	Quaternion(const quaternion_t &other) { Set(other); }
	Quaternion(const scalar_t x, const scalar_t y, const scalar_t z, const scalar_t w) { Set(x, y, z, w); }
	Quaternion(const scalar_t x, const scalar_t y, const scalar_t z) { Set(x, y, z, ScalarTraits::zero()); }
	Quaternion(const vector4_t &vec) { Set(vec[0], vec[1], vec[2], vec[3]); }
	Quaternion(const vector3_t &vec, const scalar_t w) { Set(vec[0], vec[1], vec[2], w); }
	Quaternion(const vector3_t &vec) { Set(vec[0], vec[1], vec[2], ScalarTraits::zero()); }

	bool operator==(const quaternion_t &other) { return IsEqual(other); }
	bool operator!=(const quaternion_t &other) { return !IsEqual(other); }

	quaternion_t &operator=(const quaternion_t &other) { return Set(other); }

	scalar_t &operator[](int idx) { return _data[idx]; }
	scalar_t operator[](int idx) const { return _data[idx]; }

	friend quaternion_t operator*(quaternion_t left, const quaternion_t& right) { return left.MulEq(right); }
	quaternion_t &operator*=(const quaternion_t& other) { return MulEq(other); }

	friend vector3_t operator*(quaternion_t left, const vector3_t& right)
	{
		auto inverse = left.Inverse();
		left.MulEq({ right }).MulEq(inverse);
		return{ left.x(), left.y(), left.z() };
	}

	friend quaternion_t operator*(quaternion_t quat, const scalar_t scalar) { return quat.MulEq(scalar); }
	friend quaternion_t operator*(const scalar_t scalar, quaternion_t quat) { return quat.MulEq(scalar); }
	quaternion_t &operator*=(const scalar_t scalar) { return Scale(scalar); }

	friend quaternion_t operator/(quaternion_t quat, const scalar_t scalar) { return quat.MulEq(ScalarTraits::one() / scalar); }
	quaternion_t &operator/=(const scalar_t scalar) { return Scale(ScalarTraits::one() / scalar); }

	scalar_t &x() { return _data[0]; }
	scalar_t x() const { return _data[0]; }

	scalar_t &y() { return _data[1]; }
	scalar_t y() const { return _data[1]; }

	scalar_t &z() { return _data[2]; }
	scalar_t z() const { return _data[2]; }

	scalar_t &w() { return _data[3]; }
	scalar_t w() const { return _data[3]; }

	quaternion_t &Set(const scalar_t x, const scalar_t y, const scalar_t z, const scalar_t w)
	{
		_data.Set(x, y, z, w);
		return *this;
	}

	quaternion_t &Set(const quaternion_t &other)
	{
		return Set(other.x(), other.y(), other.z(), other.w());
	}

	quaternion_t &Set(const scalar_t val)
	{
		return Set(val, val, val, val);
	}

	quaternion_t &SetIdentity()
	{
		return Set(ScalarTraits::zero(), ScalarTraits::zero(), ScalarTraits::zero(), ScalarTraits::one());
	}

	quaternion_t &SetZero()
	{
		return Set(ScalarTraits::zero(), ScalarTraits::zero(), ScalarTraits::zero(), ScalarTraits::zero());
	}

	quaternion_t &MulEq(const quaternion_t &other)
	{
		return Set(
			w() * other.x() + x() * other.w() + y() * other.z() - z() * other.y(),
			w() * other.y() + y() * other.w() + z() * other.x() - x() * other.z(),
			w() * other.z() + z() * other.w() + x() * other.y() - y() * other.x(),
			w() * other.w() - x() * other.x() - y() * other.y() - z() * other.z());
	}

	quaternion_t &Scale(const scalar_t scalar)
	{
		return Set(x() * scalar, y() * scalar, z() * scalar, w() * scalar);
	}

	quaternion_t &Normalize() { return Scale(ScalarTraits::one() / Norm()); }

	vector3_t Rotate(const vector3_t &vector) const
	{
		auto result = { *this }.MulEq(vector).MulEq((*this).Inverse());
		return{ result.x(), result.y(), result.z() };
	}

	bool IsEqual(const quaternion_t &other) const { return _data.IsEqual(other._data); }

	float NormSq() const { return _data.MagnitudeSq(); }
	float Norm() const { return _data.Magnitude(); }

	quaternion_t Normalized() const { return{ *this }.Normalize(); }

	quaternion_t Inverse() const { return{ -x(), -y(), -z(), w() }; }

	matrix_t ToRotationMatrix() const
	{
		return
		{
			w(), -z(), y(), x(),
			z(), w(), -x(), y(),
			-y(), x(), w(), z(),
			-x(), -y(), -z(), w()
		};
	}

protected:
	vector4_t _data;
};

typedef Quaternion <float> Quaternionf;
typedef Quaternion <double> Quaterniond;

END_NAMESPACE(Seizyur, Math, Geometry)

#endif // SEIZYUR_MATH_H_QUATERNION
