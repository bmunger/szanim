#ifndef SEIZYUR_MATH_H_ANGLEAXIS
#define SEIZYUR_MATH_H_ANGLEAXIS

BEGIN_NAMESPACE(Seizyur, Math, Geometry)

template<typename _SCALARTYPE>
class AngleAxis
{
public:
	typedef _SCALARTYPE ScalarType;
	typedef Vector<3, ScalarType> AxisType;

	AngleAxis(ScalarType x, ScalarType y, ScalarType z, ScalarType angle) : axis{ x, y, z }, angle{ angle } {}

	AxisType axis;
	ScalarType angle;
};

END_NAMESPACE(Seizyur, Math, Geometry)

#endif // SEIZYUR_MATH_H_ANGLEAXIS
