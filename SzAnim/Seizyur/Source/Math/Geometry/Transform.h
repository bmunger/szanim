#ifndef SEIZYUR_MATH_H_TRANSFORM
#define SEIZYUR_MATH_H_TRANSFORM

BEGIN_NAMESPACE(Seizyur, Math, Geometry)

template<typename _SCALARTYPE>
class Transform
{
	typedef _SCALARTYPE ScalarType;
	typedef Quaternion<ScalarType> QuatType;
	typedef Matrix<4, 4, ScalarType> MatType;
	typedef Vector<3, ScalarType> Vec3Type;
	typedef Vector<4, ScalarType> Vec4Type;

public:
	Transform() { SetIdentity(); }
	Transform(const Vec3Type &position, const QuatType &rotation, const Vec3Type &scale) { Set(position, rotation, scale); }
	Transform(const Vec3Type &position, const QuatType &rotation) { Set(position, rotation, Vec3Type.one()); }

	const Vec3Type &Position() const { return position; }
	void SetPosition(const Vec3Type &position) {}

	const QuatType &Rotation() const { return rotation; }
	void SetRotation(const QuatType &rotation) {}

	const Vec3Type &Scale() const { return scale; }
	void SetScale(const Vec3Type &scale) {}

	MatType ToMatrix() const
	{
		MatType result{};

		return result;
	}

	void Set(const Vec3Type &position, const QuatType &rotation, const Vec3Type &scale)
	{
		(*this).position = position;
		(*this).rotation = rotation;
		(*this).scale = scale;
	}

	void SetIdentity() { Set(Vec3Type.zero(), QuatType.identity(), Vec3Type.one()); }

private:
	QuatType rotation{};
	Vec3Type position{};
	Vec3Type scale{};
};

END_NAMESPACE(Seizyur, Math, Geometry)

#endif // SEIZYUR_MATH_H_TRANSFORM
