#ifndef SEIZYUR_MATH_H_VECTORBASE
#define SEIZYUR_MATH_H_VECTORBASE

BEGIN_NAMESPACE(Seizyur, Math, Geometry)

template<typename _DERIVED>
class VectorBase
{
private:
	template<typename T>
	using traits = Core::traits<T>;

protected:
	using vector_t = _DERIVED;
	using scalar_t = typename traits<vector_t>::scalar_t;
	using scalarTraits = traits<scalar_t>;

public:

#pragma region static methods
	//************************************************************************
	// Method:	public static zero()
	// Returns:	vector_t
	// Notes:	Returns a vector who's values are all zero.
	//************************************************************************
	static vector_t zero() { vector_t retVal; retVal.SetZero(); return retVal; }

	//************************************************************************
	// Method:	public static one()
	// Returns:	vector_t
	// Notes:	Returns a vector who's values are all one.
	//************************************************************************
	static vector_t one() { vector_t retVal; retVal.SetOne(); return retVal; }

	//************************************************************************
	// Method:	public static IsEqual()
	// Param:	const vector_t & lhs
	// Param:	const vector_t & rhs
	// Returns:	bool
	// Notes:	Returns true if the two vectors have the same values.
	//************************************************************************
	static bool IsEqual(const vector_t& lhs, const vector_t& rhs) { return lhs.IsEqual(rhs); }

	//************************************************************************
	// Method:	public static Add()
	// Param:	const vector_t & lhs
	// Param:	const vector_t & rhs
	// Returns:	vector_t
	// Notes:	Adds the two vectors and returns the result.
	//************************************************************************
	static vector_t Add(const vector_t &lhs, const vector_t& rhs) { return vector_t(lhs).Add(rhs); }

	//************************************************************************
	// Method:	public static Subtract()
	// Param:	const vector_t & lhs
	// Param:	const vector_t & rhs
	// Returns:	vector_t
	// Notes:	Subtracts the right-hand vector from the left-hand and returns
	// a copy of the result.
	//************************************************************************
	static vector_t Subtract(const vector_t &lhs, const vector_t& rhs) { return vector_t(lhs).Subtract(rhs); }

	//************************************************************************
	// Method:	public static Scale()
	// Param:	const vector_t & vec
	// Param:	const scalar_t val
	// Returns:	vector_t
	// Notes:	Returns a vector that is the result of the given vector scaled
	// by the given scalar.
	//************************************************************************
	static vector_t Scale(const vector_t &vec, const scalar_t val) { return vector_t(vec).Scale(val); }

	//************************************************************************
	// Method:	public static Scale()
	// Param:	const vector_t & lhs
	// Param:	const vector_t & rhs
	// Returns:	vector_t
	// Notes:	Returns the vector that's the result of component-wise
	// multiplication.
	//************************************************************************
	static vector_t Scale(const vector_t &lhs, const vector_t &rhs) { return vector_t(lhs).Scale(rhs); }

	//************************************************************************
	// Method:	public static Dot()
	// Param:	const vector_t & lhs
	// Param:	const vector_t & rhs
	// Returns:	scalar_t
	// Notes:	Returns the dot product of the two vectors.
	//************************************************************************
	static scalar_t Dot(const vector_t &lhs, const vector_t &rhs) { return lhs.Dot(rhs); }

	//************************************************************************
	// Method:	public static Cross()
	// Param:	const vector_t & lhs
	// Param:	const vector_t & rhs
	// Returns:	vector_t
	// Notes:	Crosses the two given vectors and returns the resulting vector.
	//************************************************************************
	static vector_t Cross(const vector_t &lhs, const vector_t &rhs) { return lhs.Cross(rhs); }

	//************************************************************************
	// Method:	public static MagnitudeSq()
	// Param:	const vector_t & vec
	// Returns:	scalar_t
	// Notes:	Returns the squared length (i.e. magnitude) of the vector.
	//************************************************************************
	static scalar_t MagnitudeSq(const vector_t &vec) { return vec.Dot(vec); }

	//************************************************************************
	// Method:	public static Magnitude()
	// Param:	const vector_t & vec
	// Returns:	scalar_t
	// Notes:	Returns the length (i.e. magnitude) of the vector.
	//************************************************************************
	static scalar_t Magnitude(const vector_t &vec) { return sqrt(vec.MagnitudeSq()); }

	//************************************************************************
	// Method:	public static Normalize()
	// Param:	const vector_t & vec
	// Returns:	vector_t
	// Notes:	Returns a normalized version of the given vector.
	//************************************************************************
	static vector_t Normalize(const vector_t &vec) { return vector_t(vec).Normalize(); }

	//************************************************************************
	// Method:	public static AngleBetween()
	// Param:	const vector_t & lhs
	// Param:	const vector_t & rhs
	// Returns:	scalar_t
	// Notes:	Returns the angle between the two vectors.
	//************************************************************************
	static scalar_t AngleBetween(const vector_t &lhs, const vector_t &rhs) { return lhs.Angle(rhs); }

	//************************************************************************
	// Method:	public static Lerp()
	// Param:	const vector_t & from
	// Param:	const vector_t & to
	// Param:	const float alpha [0-1]
	// Returns:	vector_t
	// Notes:	Returns a vector linearly interpolated between the two given
	// vectors and the given alpha [0-1].
	//************************************************************************
	static vector_t Lerp(const vector_t &from, const vector_t &to, const float alpha)
	{
		auto clampedAlpha = alpha > 1.0f ? 1.0f : alpha < 0.0f ? 0.0f : alpha;
		return (to - from) * alpha + from;
	}

	//************************************************************************
	// Method:	public static Slerp()
	// Param:	const vector_t & from
	// Param:	const vector_t & to
	// Param:	const float alpha [0-1]
	// Returns:	vector_t
	// Notes:	Returns a vector calculated using the spherical linear 
	// interpolation of the two given vectors and alpha [0-1].
	//************************************************************************
	static vector_t Slerp(const vector_t &from, const vector_t &to, const float alpha)
	{
		return Lerp(from, to, alpha);
	}
#pragma endregion

#pragma region Constructors
	template<typename ..._PTYPE>
	VectorBase(_PTYPE... args) : _data{ args... } { }

	VectorBase(const vector_t &other) : _data(other._data) { }
#pragma endregion

#pragma region Operators
	scalar_t& operator[](const int idx) { return _data(idx); }
	scalar_t operator[](const int idx) const { return _data(idx); }

	bool operator==(const vector_t& other) const { return IsEqual(other); }
	bool operator!=(const vector_t& other) const { return !IsEqual(other); }
	bool operator>(const vector_t& other) const { return MagnitudeSq() > other.MagnitudeSq(); }
	bool operator<(const vector_t& other) const { return MagnitudeSq() < other.MagnitudeSq(); }
	bool operator>=(const vector_t& other) const { return !(MagnitudeSq() < other.MagnitudeSq()); }
	bool operator<=(const vector_t& other) const { return !(MagnitudeSq() > other.MagnitudeSq()); }

	vector_t& operator=(const vector_t& other) { return Set(other); }

	vector_t& operator+=(const vector_t& other) { return Add(other); }
	friend vector_t operator+(const vector_t &lhs, const vector_t& rhs) { return Add(lhs, rhs); }

	vector_t& operator-=(const vector_t& other) { return Subtract(other); }
	friend vector_t operator-(const vector_t &lhs, const vector_t& rhs) { return Subtract(lhs, rhs); }

	vector_t& operator*=(const scalar_t val) { return Scale(val); }
	friend vector_t operator*(const scalar_t scalar, const vector_t &vec) { return Scale(vec, scalar); }
	friend vector_t operator*(const vector_t &vec, const scalar_t scalar) { return Scale(vec, scalar); }

	vector_t& operator/=(const scalar_t scalar) { return Scale(scalarTraits::one() / scalar); }
	friend vector_t operator/(const vector_t &vec, const scalar_t scalar) { return Scale(vec, scalarTraits::one() / scalar); }
#pragma endregion

	//************************************************************************
	// Method:	public static IsEqual() const
	// Param:	const vector_t & other
	// Returns:	bool
	// Notes:	Returns true if the the values in this vector match those of 
	// the given vector.
	//************************************************************************
	bool IsEqual(const vector_t& other) const { return _data.IsEqual(other._data); }

	//************************************************************************
	// Method:	public Set()
	// Param:	const scalar_t val
	// Returns:	void
	// Notes:	Sets all components in the vector to the given value.
	//************************************************************************
	void Set(const scalar_t val) { _data.Set(val); }

	//************************************************************************
	// Method:	public Set()
	// Param:	const vector_t & other
	// Returns:	void
	// Notes:	Sets the values in this vector to be the same as the given vector.
	//************************************************************************
	void Set(const vector_t &other) { _data.Set(other); }

	//************************************************************************
	// Method:	public SetZero()
	// Returns:	void
	// Notes:	Sets all components in the vector to zero.
	//************************************************************************
	void SetZero() { _data.Set(scalarTraits::zero()); }

	//************************************************************************
	// Method:	public SetOne()
	// Returns:	void
	// Notes:	Sets all components in the vector to one.
	//************************************************************************
	void SetOne() { _data.Set(scalarTraits::one()); }

	//************************************************************************
	// Method:	public static Add()
	// Param:	const vector_t & other
	// Returns:	vector_t&
	// Notes:	Adds the given vector to this one.
	//************************************************************************
	vector_t& Add(const vector_t& other)
	{
		_data.Add(other._data);
		return static_cast<vector_t&>(*this);
	}

	//************************************************************************
	// Method:	public static Subtract()
	// Param:	const vector_t & other
	// Returns:	vector_t&
	// Notes:	Subtracts the given vector from this one.
	//************************************************************************
	vector_t& Subtract(const vector_t& other)
	{
		_data.Sub(other._data);
		return static_cast<vector_t&>(*this);
	}

	//************************************************************************
	// Method:	public static Scale()
	// Param:	const scalar_t val
	// Returns:	vector_t&
	// Notes:	Multiplies each component in the vector by the given value.
	//************************************************************************
	vector_t& Scale(const scalar_t val)
	{
		_data.MulByScalar(val);
		return static_cast<vector_t&>(*this);
	}

	//************************************************************************
	// Method:	public static Scale()
	// Param:	const vector_t & other
	// Returns:	vector_t&
	// Notes:	Multiplies the two vectors, component-wise, and stores the result.
	//************************************************************************
	vector_t& Scale(const vector_t &other)
	{
		_data.MulCwise(other._data);
		return static_cast<vector_t&>(*this);
	}

	//************************************************************************
	// Method:	public static Dot() const
	// Param:	const vector_t & other
	// Returns:	scalar_t
	// Notes:	Returns the dot product of the two vectors.
	//************************************************************************
	scalar_t Dot(const vector_t &other) const
	{
		scalar_t result = scalarTraits::zero();
		for(int idx = 0; idx < traits<vector_t>::Size; idx++)
			result += _data(idx) * other._data(idx);
		return result;
	}

	//************************************************************************
	// Method:	public static Cross() const
	// Param:	const vector_t & other
	// Returns:	vector_t
	// Notes:	Crosses this vector with the given vector. result = this X other.
	//************************************************************************
	vector_t Cross(const vector_t &other) const
	{
		SZ_STATIC_ASSERT(traits<vector_t>::Size == 3, "(this) Cross product is only relevant for 3-vectors.");
		return vector_t(
			_data(1) * other._data(2) - _data(2) * other._data(1),
			_data(2) * other._data(0) - _data(0) * other._data(2),
			_data(0) * other._data(1) - _data(1) * other._data(0));
	}

	//************************************************************************
	// Method:	public static MagnitudeSq() const
	// Returns:	scalar_t
	// Notes:	Returns the squared length of the vector.
	//************************************************************************
	scalar_t MagnitudeSq() const
	{
		return Dot(*this);
	}

	//************************************************************************
	// Method:	public static Magnitude() const
	// Returns:	scalar_t
	// Notes:	Returns the length of the vector.
	//************************************************************************
	scalar_t Magnitude() const
	{
		return sqrt(MagnitudeSq());
	}

	//************************************************************************
	// Method:	public static Normalize()
	// Returns:	vector_t&
	// Notes:	Normalizes the vector. i.e. Magnitude 1.
	//************************************************************************
	vector_t& Normalize()
	{
		return Scale(scalarTraits::one() / Magnitude());
	}

	//************************************************************************
	// Method:	public Normalized() const
	// Returns:	vector_t
	// Notes:	Returns a normalized version of this vector.
	//************************************************************************
	vector_t Normalized() const
	{
		return vector_t(*this).Normalize();
	}

	//************************************************************************
	// Method:	public Angle() const
	// Param:	const vector_t & other
	// Returns:	scalar_t
	// Notes:	Returns the angle between the two vectors in degrees.
	//************************************************************************
	scalar_t Angle(const vector_t &other) const
	{
		return std::acos(Normalized().Dot(other.Normalized())) * 180.0f / M_PI;
	}

protected:
	Matrix<1, traits<vector_t>::Size, scalar_t> _data;
};

END_NAMESPACE(Seizyur, Math, Geometry)

#endif // SEIZYUR_MATH_H_VECTORBASE
