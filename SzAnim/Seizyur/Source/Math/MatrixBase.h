#ifndef SEIZYUR_MATH_H_MATRIXBASE
#define SEIZYUR_MATH_H_MATRIXBASE

BEGIN_NAMESPACE(Seizyur, Math)

template<typename _DERIVED>
class MatrixBase
{
private:
	template<typename T>
	using traits = Core::traits<T>;

public:
	using matrix_t = _DERIVED;
	using scalar_t = typename traits<matrix_t>::scalar_t;
	using row_t = typename traits<matrix_t>::row_t;
	using column_t = typename traits<matrix_t>::column_t;
	using transpose_t = typename traits<matrix_t>::transpose_t;
	using inverse_t = typename traits<matrix_t>::inverse_t;

	enum
	{
		rows = traits<matrix_t>::rows,
		columns = traits<matrix_t>::columns
	};

#pragma region Static Interface
	//************************************************************************
	// Method:	public static Diagonal()
	// Param:	const scalar_t scalar
	// Returns:	matrix_t
	// Notes:	Returns a new matrix of the derived type where the diagonal 
	//  contains the given value.
	//************************************************************************
	static matrix_t Diagonal(const scalar_t scalar)
	{
		matrix_t result;
		result.SetDiagonal(scalar);
		return result;
	}

	//************************************************************************
	// Method:	public static Identity()
	// Returns:	matrix_t
	// Notes:	Returns an identity matrix.
	//************************************************************************
	static matrix_t Identity() { return Diagonal(traits<scalar_t>::one()); }
#pragma endregion

public:
	//************************************************************************
	// Method:	public MatrixBase()
	// Param:	_PTYPE... args
	// Notes:	"Default" constructor.
	//************************************************************************
	template<typename ..._PTYPE>
	MatrixBase(_PTYPE... args) : _data{ args... } {}

	//************************************************************************
	// Method:	public MatrixBase()
	// Param:	const MatrixBase &other
	// Notes:	Copy constructor.
	//************************************************************************
// 	MatrixBase(const MatrixBase &other) : _data(other._data) {}

	//************************************************************************
	// Method:	public operator=()
	// Param:	const MatrixBase & other
	// Returns:	MatrixBase &
	//************************************************************************
	MatrixBase &operator=(const MatrixBase &other)
	{
		if(*this != other)
			_data = other._data;
		return *this;
	}

	//************************************************************************
	// Method:	public operator==() const
	// Param:	const MatrixBase & other
	// Returns:	bool
	//************************************************************************
	bool operator==(const MatrixBase &other) const { return IsEqual(other); }

	//************************************************************************
	// Method:	public operator!=() const
	// Param:	const MatrixBase & other
	// Returns:	bool
	//************************************************************************
	bool operator!=(const MatrixBase &other) const { return !IsEqual(other); }

	//************************************************************************
	// Method:	public operator()()
	// Param:	const int idx
	// Returns:	scalar_t &
	// Notes:	Returns the element in the matrix that is accessed as 1-d.
	//************************************************************************
	scalar_t &operator()(const int idx) { return _data[idx]; }

	//************************************************************************
	// Method:	public operator()() const
	// Param:	const int idx
	// Returns:	const scalar_t &
	// Notes:	Returns the element in the matrix that is accessed as 1-d.
	//************************************************************************
	scalar_t operator()(const int idx) const { return _data[idx]; }

	//************************************************************************
	// Method:	public operator()()
	// Param:	const int row
	// Param:	const int column
	// Returns:	scalar_t &
	// Notes:	Returns the element in the matrix that is accessed as 2-d.
	//************************************************************************
	scalar_t &operator()(const int row, const int column) { return _data(column, row); }

	//************************************************************************
	// Method:	public operator()() const
	// Param:	const int row
	// Param:	const int column
	// Returns:	const scalar_t &
	// Notes:	Returns the element in the matrix that is accessed as 2-d.
	//************************************************************************
	scalar_t operator()(const int row, const int column) const { return _data(column, row); }

	//************************************************************************
	// Method:	public operator+=()
	// Param:	const MatrixBase & rhs
	// Returns:	MatrixBase&
	// Notes:	Adds the given matrix to this one.
	//************************************************************************
	MatrixBase& operator+=(const MatrixBase& rhs)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] += rhs._data[i];
		return *this;
	}

	//************************************************************************
	// Method:	friend public operator+()
	// Param:	matrix_t lhs
	// Param:	const MatrixBase & rhs
	// Returns:	MatrixBase
	// Notes:	Adds the two matrices and returns the result.
	//************************************************************************
	friend matrix_t operator+(matrix_t lhs, const MatrixBase& rhs) { return (lhs += rhs); }

	//************************************************************************
	// Method:	public operator-=()
	// Param:	const MatrixBase & rhs
	// Returns:	MatrixBase&
	// Notes:	Subtracts the given matrix from this one.
	//************************************************************************
	MatrixBase& operator-=(const MatrixBase& rhs)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] -= rhs._data[i];
		return *this;
	}

	//************************************************************************
	// Method:	friend public operator-()
	// Param:	matrix_t lhs
	// Param:	const MatrixBase & rhs
	// Returns:	MatrixBase
	// Notes:	Subtracts the right hand matrix from the left hand matrix and 
	//  returns a new matrix with the result.
	//************************************************************************
	friend matrix_t operator-(matrix_t lhs, const MatrixBase& rhs) { return (lhs -= rhs); }

	//************************************************************************
	// Method:	public operator/=()
	// Param:	const scalar_t scalar
	// Returns:	matrix_t&
	// Notes:	Divides each element in this matrix by the given scalar value,
	//  storing the results. Returns a reference to this matrix.
	//************************************************************************
	matrix_t& operator/=(const scalar_t scalar)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] /= scalar;
		return static_cast<matrix_t&>(*this);
	}

	//************************************************************************
	// Method:	public operator/() const
	// Param:	const scalar_t scalar
	// Returns:	matrix_t
	// Notes:	Divides each element in this matrix by the given scalar value
	//  and returns a new matrix with the result.
	//************************************************************************
	matrix_t operator/(const scalar_t scalar) const { return (matrix_t(*this) /= scalar); }

	//************************************************************************
	// Method:	public operator*=()
	// Param:	const scalar_t scalar
	// Returns:	matrix_t&
	// Notes:	Multiplies each element in this matrix by the given scalar value,
	//  storing the results. Returns a reference to this matrix.
	//************************************************************************
	matrix_t& operator*=(const scalar_t scalar)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] *= scalar;
		return static_cast<matrix_t&>(*this);
	}

	//************************************************************************
	// Method:	public operator*()
	// Param:	const scalar_t scalar
	// Param:	const matrix_t & mat
	// Returns:	friend matrix_t
	// Notes:	Multiplies each component in this matrix by the given value.
	//************************************************************************
	friend matrix_t operator*(const scalar_t scalar, const matrix_t &mat)
	{
		return (matrix_t(mat) *= scalar);
	}

	//************************************************************************
	// Method:	public operator*() const
	// Param:	const scalar_t scalar
	// Returns:	matrix_t
	// Notes:	Multiplies each element in this matrix by the given scalar value
	//  and returns a new matrix with the result.
	//************************************************************************
	matrix_t operator*(const scalar_t scalar) const { return (matrix_t(*this) *= scalar); }

	//************************************************************************
	// Method:	public operator*=()
	// Param:	const matrix_t & rhs
	// Returns:	matrix_t&
	// Notes:	Multiplies another matrix against this one. Must have same sizes.
	//************************************************************************
	matrix_t& operator*=(const matrix_t &rhs)
	{
		static_assert(rows == columns, "Matrix *= Matrix requires square matrices of the same size.");
		(*this) = (*this) * rhs;
		return static_cast<matrix_t&>(*this);
	}

	//************************************************************************
	// Method:	public operator*()
	// Param:	const MatrixBase<_RHS> & rhs
	// Returns:	Matrix
	// Notes:	Multiplies any two matrices M[i,j] x N[j,k].
	//************************************************************************
	template <
		typename _RHS,
		typename = typename std::enable_if<Internal::is_multipliable<matrix_t, _RHS>::value >::type>
	typename Internal::matrix_outer_product_traits<matrix_t, _RHS>::result_t operator*(const MatrixBase<_RHS> &rhs)
	{
		return MulOuter(rhs);
	}

	//************************************************************************
	// Method:	public IsEqual() const
	// Param:	const MatrixBase & other
	// Returns:	bool
	// Notes:	Returns whether the matrices contain the same data.
	//************************************************************************
	bool IsEqual(const MatrixBase &other) const { return _data == other._data; }

	//************************************************************************
	// Method:	public Rows() const
	// Returns:	int
	// Notes:	Returns the number of rows in the matrix.
	//************************************************************************
	int Rows() const { return _data.Rows(); }

	//************************************************************************
	// Method:	public Columns() const
	// Returns:	int
	// Notes:	Returns the number of columns in the matrix.
	//************************************************************************
	int Columns() const { return _data.Columns(); }

	//************************************************************************
	// Method:	public GetRow() const
	// Param:	const int rowIdx
	// Returns:	row_t
	// Notes:	Returns a copy of the requested row.
	//************************************************************************
	row_t GetRow(const int rowIdx) const
	{
		row_t result{};
		for(int idx = 0; idx < Columns(); idx++)
			result(idx) = (*this)(rowIdx, idx);
		return result;
	}

	//************************************************************************
	// Method:	public GetColumn() const
	// Param:	const int colIdx
	// Returns:	column_t
	// Notes:	Returns a copy of the requested column.
	//************************************************************************
	column_t GetColumn(const int colIdx) const
	{
		column_t result{};
		for(int idx = 0; idx < Rows(); idx++)
			result(idx) = (*this)(idx, colIdx);
		return result;
	}

	//************************************************************************
	// Method:	public GetTranspose() const
	// Returns:	TransposeType
	// Notes:	Returns a transposed version of the matrix.
	//************************************************************************
	transpose_t GetTranspose() const
	{
		transpose_t transpose{};
		for(int i = 0; i < Rows(); i++)
			for(int j = 0; j < Columns(); j++)
				transpose(j, i) = (*this)(i, j);
		return transpose;
	}

	//************************************************************************
	// Method:	public Set()
	// Param:	const scalar_t scalar
	// Returns:	void
	// Notes:	Sets all coefficients to the requested value.
	//************************************************************************
	void Set(const scalar_t scalar)
	{
		for(int rowIdx = 0; rowIdx < Rows(); rowIdx++)
			for(int colIdx = 0; colIdx < Columns(); colIdx++)
				(*this)(rowIdx, colIdx) = scalar;
	}

	//************************************************************************
	// Method:	public Set()
	// Param:	const matrix_t & other
	// Returns:	void
	// Notes:	Sets the 
	//************************************************************************
	void Set(const matrix_t &other) { Set(0, 0, other, 0, 0, Rows(), Columns()); }

	//************************************************************************
	// Method:	public Set()
	// Param:	const int startRow
	// Param:	const int startCol
	// Param:	const MatrixBase & other
	// Param:	const int otherStartRow
	// Param:	const int otherStartCol
	// Param:	const int width
	// Param:	const int height
	// Returns:	void
	// Notes:	Sets the requested subset of the other matrix to the requested.
	//  offset of this matrix.
	//************************************************************************
	void Set(const int startRow, const int startCol, const MatrixBase &other, const int otherStartRow, const int otherStartCol, const int width, const int height)
	{
		for(int rowIdx = 0; rowIdx < height; rowIdx++)
			for(int colIdx = 0; colIdx < width; colIdx++)
				(*this)(startRow + rowIdx, startCol + colIdx) = other(otherStartRow + rowIdx, otherStartCol + colIdx);
	}

	//************************************************************************
	// Method:	public SetDiagonal()
	// Param:	const scalar_t scalar
	// Returns:	void
	// Notes:	Sets the diagonal elements of this matrix to the given value.
	//  Sets all other elements to zero.
	//************************************************************************
	void SetDiagonal(const scalar_t scalar)
	{
		for(int rowIdx = 0; rowIdx < Rows(); rowIdx++)
			for(int colIdx = 0; colIdx < Columns(); colIdx++)
				(*this)(rowIdx, colIdx) = (rowIdx == colIdx) ? scalar : 0;
	}

	//************************************************************************
	// Method:	public SetZero()
	// Returns:	void
	// Notes:	Sets all elements in the matrix to zero.
	//************************************************************************
	void SetZero() { Set(traits<scalar_t>::zero()); }

	//************************************************************************
	// Method:	public SetIdentity()
	// Returns:	void
	// Notes:	Sets this matrix to the identity matrix.
	//************************************************************************
	void SetIdentity() { SetDiagonal(traits<scalar_t>::one()); }

	//************************************************************************
	// Method:	public Add()
	// Param:	const MatrixBase<matrix_t> & other
	// Returns:	MatrixBase<matrix_t> &
	// Notes:	Adds 'other' to this matrix.
	//************************************************************************
	MatrixBase<matrix_t> &Add(const MatrixBase<matrix_t> &other)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] += other._data[i];
		return *this;
	}

	//************************************************************************
	// Method:	public Sub()
	// Param:	const MatrixBase<matrix_t> & other
	// Returns:	MatrixBase<matrix_t> &
	// Notes:	Subtracts 'other' from this matrix.
	//************************************************************************
	MatrixBase<matrix_t> &Sub(const MatrixBase<matrix_t> &other)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] -= other._data[i];
		return *this;
	}

	//************************************************************************
	// Method:	public MulByScalar()
	// Param:	const scalar_t scalar
	// Returns:	MatrixBase<matrix_t> &
	// Notes:	Multiplies each component in the matrix by the given scalar.
	//************************************************************************
	MatrixBase<matrix_t> &MulByScalar(const scalar_t scalar)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] *= scalar;
		return *this;
	}

	//************************************************************************
	// Method:	public MulCwise()
	// Param:	const MatrixBase<matrix_t> & rhs
	// Returns:	MatrixBase<matrix_t> &
	// Notes:	Multiplies each component in this matrix by the matching component in rhs.
	//************************************************************************
	MatrixBase<matrix_t> &MulCwise(const MatrixBase<matrix_t> &rhs)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] *= rhs._data[i];
		return *this;
	}

	//************************************************************************
	// Method:	public MulOuter()
	// Param:	const MatrixBase<_RHS> & rhs
	// Returns:	Multiplied matrix.
	// Notes:	Multiplies any two matrices M[i,j] x N[j,k].
	//************************************************************************
	template<typename _RHS, typename = typename std::enable_if<Internal::is_matrix<_RHS>::value && Internal::is_multipliable<matrix_t, _RHS>::value>::type>
	typename Internal::matrix_outer_product_traits<matrix_t, _RHS>::result_t 
		MulOuter(const MatrixBase<_RHS> &rhs)
	{
		typename Internal::matrix_outer_product_traits<matrix_t, _RHS>::result_t result;

		for(int rowIdx = 0; rowIdx < result.rows; rowIdx++)
		{
			for(int colIdx = 0; colIdx < result.columns; colIdx++)
			{
				scalar_t val{ };
				for(int i = 0; i < columns; i++)
					val += (*this)(rowIdx, i) * rhs(i, colIdx);
				result(rowIdx, colIdx) = val;
			}
		}

		return result;
	}

	//************************************************************************
	// Method:	public DivByScalar()
	// Param:	const scalar_t scalar
	// Returns:	MatrixBase<matrix_t> &
	// Notes:	Divided each component in the matrix by the given scalar.
	//************************************************************************
	MatrixBase<matrix_t> &DivByScalar(const scalar_t scalar)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] /= scalar;
		return *this;
	}

	//************************************************************************
	// Method:	public DivCwise()
	// Param:	const MatrixBase<matrix_t> & rhs
	// Returns:	MatrixBase<matrix_t> &
	// Notes:	Divides each component in this matrix by the matching component in rhs
	//************************************************************************
	MatrixBase<matrix_t> &DivCwise(const MatrixBase<matrix_t> &rhs)
	{
		for(int i = 0; i < _data.Size(); i++)
			_data[i] /= rhs._data[i];
		return *this;
	}

protected:
	typename traits<matrix_t>::storage_t _data;
};

END_NAMESPACE(Seizyur, Math)
#endif // SEIZYUR_MATH_H_MATRIXBASE
