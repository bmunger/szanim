#ifndef SEIZYUR_MATH_H_MATRIX
#define SEIZYUR_MATH_H_MATRIX

BEGIN_NAMESPACE(Seizyur, Math)

template<int _ROWS, int _COLUMNS, typename _SCALARTYPE>
class Matrix : public MatrixBase < Matrix<_ROWS, _COLUMNS, _SCALARTYPE> >
{
public:
	using MatrixBase::MatrixBase;

	//////////////////////////////////////////////////////////////////////////
	// GetPivotMatrix
	//	Given an NxM matrix, this will give the permutation matrix that can
	//	mutate this one into one that is partially pivoted.
	//////////////////////////////////////////////////////////////////////////
	matrix_t GetPivotMatrix() const
	{
		matrix_t pivotMatrix = Identity();

		// Start at the top-left
		for(int i = 0; i < rows; i++)
		{
			// Look through all rows after i to find the largest one.
			int maxRowIdx = i;
			for(int j = i; j < rows; j++)
			{
				if((*this)(j, i) > (*this)(maxRowIdx, i))
					maxRowIdx = j;
			}

			// If we found a row with a larger value than what is at (i,i),
			// swap row i with row maxRowIdx.
			if(maxRowIdx != i)
			{
				for(int k = 0; k < columns; k++)
				{
					scalar_t tmp = pivotMatrix(i, k);
					pivotMatrix(i, k) = pivotMatrix(maxRowIdx, k);
					pivotMatrix(maxRowIdx, k) = tmp;
				}
			}
		}
		return pivotMatrix;
	}

	//////////////////////////////////////////////////////////////////////////
	// Inverse
	//  Given an m-by-n matrix, A if will have an inverse n-by-m matrix B if:
	//	 - It has a non-zero determinant.
	//		AND
	//	 - Has m == n:  BA = AB = I.
	//	 - OR has rank n, it has a left inverse: an n-by-m matrix B such that BA = I.
	//	 - OR has rank m, it has a right inverse: an n-by-m matrix B such that AB = I.
	//////////////////////////////////////////////////////////////////////////
	inverse_t Inverse() const
	{
		matrix_t L{ }, U{ }, P{ };
		Doolittle(P, L, U);

		inverse_t result{ };
		column_t vector_b{ };

		for(int i = 0; i < columns; i++)
		{
			vector_b.SetZero();
			vector_b(i) = traits<scalar_t>::one();

			auto vector_x = Solve(vector_b);
			for(int j = 0; j < rows; j++)
				result(j, i) = vector_x(j);
		}
		return result;
	}

	//////////////////////////////////////////////////////////////////////////
	// Solves Ax=b. Vector, x, is the vector being solved for and is returned.
	//////////////////////////////////////////////////////////////////////////
	column_t Solve(column_t vec_b, matrix_t &pivot, matrix_t &lower, matrix_t &upper) const
	{
		column_t vec_x{ };
		column_t vec_d{ };

		// Forward substitute
		vec_d(0) = vec_b(0);
		for(int idx = 1; idx < rows; idx++)
		{
			vec_d(idx) = vec_b(idx);
			for(int n = 1; n < idx - 1; n++)
				vec_d(idx) -= vec_d(n) * lower(idx, n);
		}

		// Back substitute
		vec_x(rows - 1) = vec_d(rows - 1) / upper(rows - 1, rows - 1);
		for(int idx = rows - 2; idx > 0; idx--)
		{
			vec_x(idx) = vec_d(idx);
			for(int n = idx; n < rows - 2; n++)
				vec_x(idx) -= upper(idx, n) * vec_x(n);
			vec_x(idx) /= upper(idx, idx);
		}

		return vec_x;
	}
	column_t Solve(column_t vec_b) const
	{
		matrix_t P{ }, L{ }, U{ };
		Doolittle(P, L, U);

		return Solve(vec_b, P, L, U);
	}

	//////////////////////////////////////////////////////////////////////////
	// LU Decomposition
	// - http://rosettacode.org/wiki/LU_decomposition
	// - http://en.wikipedia.org/wiki/LU_decomposition
	//////////////////////////////////////////////////////////////////////////
	void Doolittle(matrix_t &lower, matrix_t &upper) const
	{
		matrix_t pivot{ };
		Doolittle(pivot, lower, upper);
	}

	void Doolittle(matrix_t &pivot, matrix_t &lower, matrix_t &upper) const
	{
		pivot.SetIdentity();
		lower.SetIdentity();
		upper._data = _data;

		// Loop through each column
		for(int colIdx = 0; colIdx < columns; colIdx++)
		{
			// Loop through all rows in the column below the diagonal.
			for(int rowIdx = colIdx + 1; rowIdx < rows; rowIdx++)
			{
				// M(row,col) = -U(row,col) / U(col,col)
				// and L(row,col) = -(M^-1)(row,col)
				// therefore L(row,col) = U(row,col) / U(col,col)
				lower(rowIdx, colIdx) = upper(rowIdx, colIdx) / upper(colIdx, colIdx);

				// Loop through all columns right of colIdx on row rowIdx and apply L(row,col) to U(row,n).
				for(int colOpIdx = colIdx; colOpIdx < columns; colOpIdx++)
					upper(rowIdx, colOpIdx) -= upper(colIdx, colOpIdx) * lower(rowIdx, colIdx);
			}
		}
	}
};
END_NAMESPACE(Seizyur, Math)
#endif // SEIZYUR_MATH_H_MATRIX
