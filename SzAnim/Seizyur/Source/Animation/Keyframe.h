#ifndef SEIZYUR_ANIM_H_KEYFRAME
#define SEIZYUR_ANIM_H_KEYFRAME

BEGIN_NAMESPACE(Seizyur, Animation)

template<typename _VALUETYPE>
class Keyframe
{
	typedef _VALUETYPE ValueType;
public:
	Keyframe() :_time{ 0.0f }, _value{ traits<ValueType>::zero() } {}
	Keyframe(float time, ValueType value) :_time{ time }, _value{ value } {}

	Keyframe &operator=(const Keyframe<ValueType> &other) { _time = other._time; _value = other._value; return *this; }
	bool operator>(const Keyframe<ValueType> &other) { return _time - other._time > traits<float>::eps(); }
	bool operator<(const Keyframe<ValueType> &other) { return other._time - _time > traits<float>::eps(); }
	bool operator>=(const Keyframe<ValueType> &other) { return _time - other._time > -traits<float>::eps(); }
	bool operator<=(const Keyframe<ValueType> &other) { return other._time - _time > -traits<float>::eps(); }
	bool operator==(const Keyframe<ValueType> &other) { return abs(other._time - _time) < traits<float>::eps(); }

	void Set(const float time, const ValueType &value) { _time = time; _value = value; }

	ValueType Value() const { return _value; }
	ValueType &ValueRef() { return _value; }
	float Time() const { return _value; }
	float &TimeRef() { return _value; }

private:
	ValueType _value;
	float _time;
};

END_NAMESPACE(Seizyur, Animation)
#endif