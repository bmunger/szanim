#ifndef SEIZYUR_ANIM_H_BONE
#define SEIZYUR_ANIM_H_BONE

BEGIN_NAMESPACE(Seizyur, Animation)

template<typename _SCALARTYPE, int Dim>
class Bone
{
	typedef _SCALARTYPE ScalarType;

	typedef Math::Geometry::Vector<Dim, ScalarType> VectorType;
	typedef Math::Geometry::Vector<Dim + 1, ScalarType> HmgVectorType;

public:
	const int Parent() const { return _parent; }
	const std::string &Name() const { return _name; }

	const VectorType &Translation() const { return VectorType::zero(); }
	void SetTranslation(const VectorType &translation) {}

private:
	int _parent;
	std::string _name;
	Math::Geometry::Transform _transform;
};

END_NAMESPACE(Seizyur, Animation)
#endif