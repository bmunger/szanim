#ifndef SEIZYUR_ANIM_H_TRACK
#define SEIZYUR_ANIM_H_TRACK

BEGIN_NAMESPACE(Seizyur, Animation)

template<typename _DATATYPE>
class Track
{
	typedef _DATATYPE DataType;
	typedef Keyframe<DataType> KeyType;

public:
	Track() { _keys.clear(); }
	Track(const std::vector<KeyType> &keys) { _keys = keys; }

	KeyType &operator[](int index) { return _keys[index]; }

	int NumKeys() const { return _keys.count(); }
	bool IsEmpty() const { return _keys.count() == 0; }

	void Add(const float time, const DataType &value) {}
	void Add(const KeyType &key) {}

	void Remove(const int index) {}
	void RemoveRange(const int startIndex, const int count) {}
	void Clear() {}

	void Sort() {}

	DataType Evaluate(float time) { return traits<DataType>::zero(); }

private:
	std::vector<KeyType> _keys;
};

END_NAMESPACE(Seizyur, Animation)
#endif