#ifndef SEIZYUR_CORE_H_TRAITS
#define SEIZYUR_CORE_H_TRAITS

BEGIN_NAMESPACE(Seizyur, Core)
template<typename T, typename _ENABLE = void> struct _traits;

template<int _WIDTH, int _HEIGHT, typename _DATATYPE>
struct _traits < Array2d<_WIDTH, _HEIGHT, _DATATYPE> >
{
	using type = Array2d<_WIDTH, _HEIGHT, _DATATYPE>;

	using array_t = _DATATYPE[_WIDTH * _HEIGHT];
	using value_t = _DATATYPE;

	enum
	{
		width = _WIDTH,
		height = _HEIGHT,
		size = _WIDTH * _HEIGHT
	};
};

template<typename T>
struct traits
	: _traits < typename std::remove_cv<T>::type >
{ };
END_NAMESPACE(Seizyur, Core)

#endif // SEIZYUR_CORE_H_TRAITS
