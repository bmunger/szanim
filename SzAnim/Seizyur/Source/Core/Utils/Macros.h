#ifndef SEIZYUR_UTILS_H_MACROS
#define SEIZYUR_UTILS_H_MACROS

//////////////////////////////////////////////////////////////////////////
// Macros to support overloading. See BEGIN_NAMESPACE() for an example.
////////////////////////////////////////////////////////////////////////// 
#define GLUE(x, y) x y

#define OVERLOAD_MACRO2(name, suffix) name##suffix
#define OVERLOAD_MACRO1(name, suffix) OVERLOAD_MACRO2(name, suffix)
#define OVERLOAD_MACRO(name, suffix) OVERLOAD_MACRO1(name, suffix)

#define RETURN_ARG_MAX5(_1_, _2_, _3_, _4_, _5_, arg, ...) arg
#define EXPAND_ARGS(args) RETURN_ARG_MAX5 args
#define GET_COUNT_ARGS_MAX5(...) EXPAND_ARGS((__VA_ARGS__, 5, 4, 3, 2, 1, 0))

#define CALL_OVERLOAD(name, ...) GLUE(OVERLOAD_MACRO(name, GET_COUNT_ARGS_MAX5(__VA_ARGS__)), (__VA_ARGS__))

//////////////////////////////////////////////////////////////////////////
// Namespace macros. Prevents the editor from auto-indenting namespaces.
////////////////////////////////////////////////////////////////////////// 
#define BEGIN_NS1(x) namespace x {
#define BEGIN_NS2(x, y) namespace x { namespace y {
#define BEGIN_NS3(x, y, z) namespace x { namespace y { namespace z { 
#define BEGIN_NS4(x, y, z, m) namespace x { namespace y { namespace z { namespace m { 
#define BEGIN_NS5(x, y, z, m, n) namespace x { namespace y { namespace z { namespace m { namespace n { 
#define BEGIN_NAMESPACE(...) CALL_OVERLOAD(BEGIN_NS, __VA_ARGS__)

#define END_NS1(x) }
#define END_NS2(x, y) } }
#define END_NS3(x, y, z) } } }
#define END_NS4(x, y, z, m) } } } }
#define END_NS5(x, y, z, m, n) } } } } }
#define END_NAMESPACE(...) CALL_OVERLOAD(END_NS, __VA_ARGS__)

//////////////////////////////////////////////////////////////////////////
// Assertion and debug macros.
//////////////////////////////////////////////////////////////////////////
#ifdef SEIZYUR_DEBUG
	#ifdef SEIZYUR_UNITTEST
		#define SZ_DEBUG_MSG(message) do { std::ostringstream ostr; ostr << message; Microsoft::VisualStudio::CppUnitTestFramework::Logger::WriteMessage(ostr.str().c_str()); } while(false)
		#define SZ_DEBUG_VAR(var) do { SZ_DEBUG_MSG( #var << ": " << var); } while(false)
		#define SZ_DEBUG_ERR(message) do { SZ_DEBUG_MSG(message); } while(false)
	#else
		#define SZ_DEBUG_VAR(var) do { std::cout << #var << ": " << var << std::endl; } while(false)
		#define SZ_DEBUG_MSG(message) do { std::cout << message << std::endl; } while(false)
		#define SZ_DEBUG_ERR(message) do { std::cerr << message << std::endl; } while(false)
	#endif // SEIZYUR_UNITTEST

	#define SZ_STATIC_ASSERT(condition, message) do { static_assert(condition, message); } while(false)

	#define SZ_ASSERT(condition, message) \
		do { \
			if(!(condition)) { \
				SZ_DEBUG_MSG("Assertion `" #condition "` failed in " << __FILE__ << " line " << __LINE__ << ": " << message); \
				assert(condition); \
		} } while(false)

	#define SZ_ASSERT_ARRAY_1D(size, idx) \
		do { \
			if(!(idx >= 0 && idx < size)) { \
				SZ_ASSERT(idx >= 0 && idx < size, "\n\tIndexed array out of bounds: size(" << size << ") idx(" << idx << ")"); \
		} } while(false)

	#define SZ_ASSERT_ARRAY_2D(width, height, xIdx, yIdx) \
		do { \
			if(!(xIdx >= 0 && yIdx >= 0 && xIdx < width && yIdx < height)) { \
				SZ_ASSERT((xIdx >= 0 && yIdx >= 0 && xIdx < width && yIdx < height), "\n\tIndexed array out of bounds: size(" << width << ", " << height << ") Idx(" << xIdx << ", " << yIdx << ")"); \
		} } while(false)

#else
	#define SZ_DEBUG_VAR(var) do { } while (false)
	#define SZ_DEBUG_MSG(message) do { } while (false)
	#define SZ_DEBUG_ERR(message) do { } while (false)

	#define SZ_ASSERT(condition, message) do { } while (false)
	#define SZ_STATIC_ASSERT(condition, message) do { } while (false)

	#define SZ_ASSERT_ARRAY_1D(size, idx) do { } while (false)
	#define SZ_ASSERT_ARRAY_2D(width, height, xIdx, yIdx) do { } while (false)
#endif // SEIZYUR_DEBUG

#endif // SEIZYUR_UTILS_H_MACROS
