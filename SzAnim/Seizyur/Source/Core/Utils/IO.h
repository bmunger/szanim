#ifndef SEIZYUR_CORE_H_IO
#define SEIZYUR_CORE_H_IO

BEGIN_NAMESPACE(Seizyur, Debug)

template<int _WIDTH, int _HEIGHT, typename _DATATYPE>
std::ostream& operator<<(std::ostream& out, const Core::Array2d<_WIDTH, _HEIGHT, _DATATYPE>& obj)
{
	out << std::setprecision(3) << std::setfill(' ') << std::right;

	for(int yIdx = 0; yIdx < _HEIGHT; yIdx++)
	{
		out << "[";
		for(int xIdx = 0; xIdx < _WIDTH; xIdx++)
		{
			out << std::setw(4) << obj[yIdx*_WIDTH + xIdx]
				<< ((xIdx == _WIDTH - 1) ? "]" : ", ");
		}
		if(yIdx < _HEIGHT - 1)
			out << "\n";
	}
	return out;
}

END_NAMESPACE(Seizyur, Debug)
#endif // SEIZYUR_CORE_H_IO