#ifndef SEIZYUR_UTILS_H_DEBUG
#define SEIZYUR_UTILS_H_DEBUG

BEGIN_NAMESPACE(Seizyur, Debug)
using namespace std;

inline void PrintIndent(std::ostream& out, int size, char ch = ' ')
{
	int count = size < 0 ? 0 : size;
	for(int idx = 0; idx < count; idx++)
		out << ch;
}
inline void PrintIndent(int size, char ch = ' ')
{
	PrintIndent(std::cout, size, ch);
}

inline void PrintSeparator(std::ostream& out, char separatorChar = '-', int indentLevel = 0)
{
	out << endl;
	PrintIndent(indentLevel);
	out << setfill(separatorChar) << setw(80) << separatorChar << endl;
}
inline void PrintSeparator(char separatorChar = '-', int indentLevel = 0)
{
	PrintSeparator(std::cout, separatorChar, indentLevel);
}

inline void PrintHeading(std::ostream& out, const char *description, char separatorChar = '-', int indentLevel = 0)
{
	PrintSeparator(out, separatorChar, indentLevel);
	PrintIndent(out, indentLevel);
	out << separatorChar << separatorChar << " " << description;
	PrintSeparator(out, separatorChar, indentLevel);
}
inline void PrintHeading(const char *description, char separatorChar = '-', int indentLevel = 0)
{
	PrintHeading(std::cout, description, separatorChar, indentLevel);
}

inline void PrintTitle(std::ostream& out, const char *description) { PrintHeading(out, description, '=', 0); }
inline void PrintTitle(const char *description) { PrintHeading(std::cout, description, '=', 0); }

inline void PrintHeading1(std::ostream& out, const char *description) { PrintHeading(out, description, '-', 0); }
inline void PrintHeading1(const char *description) { PrintHeading(std::cout, description, '-', 0); }

inline void PrintHeading2(std::ostream& out, const char *description) { PrintHeading(out, description, '-', 1); }
inline void PrintHeading2(const char *description) { PrintHeading(std::cout, description, '-', 1); }

template<typename _T>
inline void Print(std::ostream& out, const char* description, const _T& obj)
{
	out << description << ":\n" << obj << endl;
}

template<typename _T>
inline void Print(const char* description, const _T& obj)
{
	Print<_T>(std::cout, description, obj);
}

END_NAMESPACE(Seizyur, Debug)
#endif // SEIZYUR_UTILS_H_DEBUG