#ifndef SEIZYUR_CORE_H_FWDDECL
#define SEIZYUR_CORE_H_FWDDECL

BEGIN_NAMESPACE(Seizyur, Core)

template<typename T, typename _ENABLE>
struct _traits;

template<typename T>
struct traits;

END_NAMESPACE(Seizyur, Core)

#endif // SEIZYUR_CORE_H_FWDDECL
