#ifndef SEIZYUR_CORE_H_ARRAY2D
#define SEIZYUR_CORE_H_ARRAY2D

BEGIN_NAMESPACE(Seizyur, Core)

template<int _WIDTH, int _HEIGHT, typename _DATATYPE>
class Array2d
{
private:
	using array2d_t = Array2d<_WIDTH, _HEIGHT, _DATATYPE>;
	using value_t = typename traits<array2d_t>::value_t;

public:
	template<typename ..._PTYPE>
	Array2d(_PTYPE&&... args) : _data { args... } { }

	//************************************************************************
	// Method:	public operator==() const
	// Param:	const Array2d & other
	// Returns:	bool
	// Notes:	True if the objects have matching data.
	//************************************************************************
	inline bool operator==(const Array2d &other) const { return IsEqual(other); }

	//************************************************************************
	// Method:	public operator!=() const
	// Param:	const Array2d & other
	// Returns:	bool
	// Notes:	True if the objects do not have matching data.
	//************************************************************************
	inline bool operator!=(const Array2d &other) const { return !IsEqual(other); }

	//************************************************************************
	// Method:	public operator[]()
	// Param:	int idx
	// Returns:	value_t &
	// Notes:	Returns the element at the given index.
	//************************************************************************
	inline value_t &operator[](int idx)
	{
		SZ_ASSERT_ARRAY_1D(traits<array2d_t>::size, idx);
		return _data[idx];
	}

	//************************************************************************
	// Method:	public operator[]() const
	// Param:	int idx
	// Returns:	const value_t &
	// Notes:	Returns the element at the given index.
	//************************************************************************
	inline value_t operator[](int idx) const
	{
		SZ_ASSERT_ARRAY_1D(traits<array2d_t>::size, idx);
		return _data[idx];
	}

	//************************************************************************
	// Method:	public operator()()
	// Param:	int idx
	// Returns:	value_t &
	// Notes:	Returns the element at the given index.
	//************************************************************************
	inline value_t &operator()(int idx)
	{
		SZ_ASSERT_ARRAY_1D(traits<array2d_t>::size, idx);
		return _data[idx];
	}

	//************************************************************************
	// Method:	public operator()() const
	// Param:	int idx
	// Returns:	const value_t &
	// Notes:	Returns the element at the given index.
	//************************************************************************
	inline value_t operator()(int idx) const
	{
		SZ_ASSERT_ARRAY_1D(traits<array2d_t>::size, idx);
		return _data[idx];
	}

	//************************************************************************
	// Method:	public operator()()
	// Param:	int xIdx
	// Param:	int yIdx
	// Returns:	value_t &
	// Notes:	Returns the element at the given two dimensional index.
	//************************************************************************
	inline value_t &operator()(int xIdx, int yIdx)
	{
		SZ_ASSERT_ARRAY_2D(traits<array2d_t>::width, traits<array2d_t>::height, xIdx, yIdx);
		return _data[xIdx + yIdx * _WIDTH];
	}

	//************************************************************************
	// Method:	public operator()() const
	// Param:	int xIdx
	// Param:	int yIdx
	// Returns:	const value_t &
	// Notes:	Returns the element at the given two dimensional index.
	//************************************************************************
	inline value_t operator()(int xIdx, int yIdx) const
	{
		SZ_ASSERT_ARRAY_2D(traits<array2d_t>::width, traits<array2d_t>::height, xIdx, yIdx);
		return _data[xIdx + yIdx * _WIDTH];
	}

	//************************************************************************
	// Method:	public Rows() const
	// Returns:	int
	// Notes:	Returns the number of rows in the data structure.
	//************************************************************************
	inline int Rows() const { return traits<array2d_t>::height; }

	//************************************************************************
	// Method:	public Columns() const
	// Returns:	int
	// Notes:	Returns the number of columns in the data structure.
	//************************************************************************
	inline int Columns() const { return traits<array2d_t>::width; }

	//************************************************************************
	// Method:	public Width() const
	// Returns:	int
	// Notes:	Returns the width the data structure.
	//************************************************************************
	inline int Width() const { return traits<array2d_t>::width; }
	//************************************************************************
	// Method:	public Height() const
	// Returns:	int
	// Notes:	Returns the height the data structure.
	//************************************************************************
	inline int Height() const { return traits<array2d_t>::height; }

	//************************************************************************
	// Method:	public Size() const
	// Returns:	int
	// Notes:	Returns the total number of elements in the data structure.
	//************************************************************************
	inline int Size() const { return traits<array2d_t>::size; }

	//************************************************************************
	// Method:	public IsEqual() const
	// Param:	const Array2d & other
	// Returns:	bool
	// Notes:	Returns true if the data structures have matching values.
	//************************************************************************
	inline bool IsEqual(const Array2d &other) const
	{
		for(int i = 0; i < Size(); i++)
		{
			if(_data[i] != other._data[i])
				return false;
		}
		return true;
	}

	//************************************************************************
	// Method:	public Zero()
	// Returns:	void
	// Notes:	zeros the array.
	//************************************************************************
	inline void Zero() { Fill(traits<value_t>::zero(), 0, traits<array2d_t>::size); }

	//************************************************************************
	// Method:	public Zero()
	// Param:	const int offset
	// Param:	const int count
	// Returns:	void
	// Notes:	zeros out the requested block of data (1d).
	//************************************************************************
	inline void Zero(const int offset, const int count) { Fill(traits<value_t>::zero(), offset, count); }

	//************************************************************************
	// Method:	public Zero()
	// Param:	const int xOffset
	// Param:	const int yOffset
	// Param:	const int width
	// Param:	const int height
	// Returns:	void
	// Notes:	zeros the requested block of data (2d).
	//************************************************************************
	inline void Zero(const int xOffset, const int yOffset, const int width, const int height)
	{
		Fill(traits<value_t>::zero(), xOffset, yOffset, width, height);
	}

	//************************************************************************
	// Method:	public Fill()
	// Param:	const value_t val
	// Returns:	void
	// Notes:	Sets the data to the given value.
	//************************************************************************
	inline void Fill(const value_t val) { Fill(val, 0, traits<array2d_t>::size); }

	//************************************************************************
	// Method:	public Fill()
	// Param:	const value_t val
	// Param:	const int offset
	// Param:	const int count
	// Returns:	void
	// Notes:	Fills the requested portion of the data with the given value.
	//************************************************************************
	inline void Fill(const value_t val, const int offset, const int count)
	{
		SZ_ASSERT(offset >= 0 && count >= 0 && offset + count <= traits<array2d_t>::size, "");
		for(int i = offset; i < offset + count; i++)
			_data[i] = val;
	}

	//************************************************************************
	// Method:	public Fill()
	// Param:	const value_t val
	// Param:	const int xOffset
	// Param:	const int yOffset
	// Param:	const int width
	// Param:	const int height
	// Returns:	void
	// Notes:	Fills the requested block of data with the given value.
	//************************************************************************
	inline void Fill(const value_t val, const int xOffset, const int yOffset, const int width, const int height)
	{
		SZ_ASSERT(xOffset >= 0 && yOffset >= 0 && xOffset + width <= traits<array2d_t>::width && yOffset + height <= traits<array2d_t>::height, "");
		for(int i = yOffset; i < yOffset + height; i++)
			Fill(val, i*traits<array2d_t>::width + xOffset, width);
	}

private:

	//************************************************************************
	// Variable: array_t _data
	// Notes:	 The data.
	//************************************************************************
	typename traits<array2d_t>::array_t _data;
};
END_NAMESPACE(Seizyur, Core)
#endif // SEIZYUR_CORE_H_ARRAY2D
