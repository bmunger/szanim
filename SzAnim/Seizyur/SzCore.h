#ifndef SEIZYUR_H_CORE
#define SEIZYUR_H_CORE

#include "Source/Core/Utils/Macros.h"

#ifdef SEIZYUR_DEBUG
#include <iostream>
#include <iomanip>
#include <cassert>

#include "Source/Core/Utils/Debug.h"
#endif

#include "Source/Core/FwdDecl.h"

#include "Source/Core/Array2d.h"
#include "Source/Core/Traits.h"

#ifdef SEIZYUR_DEBUG
#include "Source/Core/Utils/IO.h"
#endif

#endif // SEIZYUR_H_CORE
