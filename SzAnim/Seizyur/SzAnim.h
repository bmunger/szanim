#ifndef SEIZYUR_H_ANIM
#define SEIZYUR_H_ANIM

#include "Source/Animation/Keyframe.h"
#include "Source/Animation/Track.h"
#include "Source/Animation/Bone.h"
#include "Source/Animation/Skeleton.h"
#include "Source/Animation/Clip.h"
#include "Source/Animation/Animation.h"

#endif // SEIZYUR_H_ANIM
