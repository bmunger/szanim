#ifndef SEIZYUR_H_MATH
#define SEIZYUR_H_MATH

#include "Source/Math/Functions.h"

#include "Source/Math/Internal/FwdDecl.h"

#include "Source/Math/Internal/Meta.h"
#include "Source/Math/Internal/UnaryOpTraits.h"
#include "Source/Math/Internal/BinaryOpTraits.h"

#include "Source/Math/MatrixBase.h"
#include "Source/Math/Matrix.h"

#include "Source/Math/Geometry/VectorBase.h"
#include "Source/Math/Geometry/Vector.h"

#include "Source/Math/Geometry/Quaternion.h"
#include "Source/Math/Geometry/Transform.h"
#include "Source/Math/Geometry/AngleAxis.h"

#include <vector> // TODO: Refactor to not use std::vector.
#include "Source/Math/Spline.h"

#ifdef SEIZYUR_DEBUG
#include "Source/Math/Utils/IO.h"
#endif

#include "Source/Math/Traits.h"

#endif // SEIZYUR_H_MATH
